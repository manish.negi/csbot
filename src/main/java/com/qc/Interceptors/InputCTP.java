package com.qc.Interceptors;

import java.util.Map;

public interface InputCTP 
{

	public String getInputCTP(String sessionId, Map<String, Map> responsecache_onsessionId);

}
