package com.qc.Interceptors;

import java.util.Map;

public interface InputFund {

	public String getInputFund(String sessionId, Map<String, Map> responsecache_onsessionId);

}
