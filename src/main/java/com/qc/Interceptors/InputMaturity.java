package com.qc.Interceptors;

import java.util.Map;

public interface InputMaturity 
{

	public String getInputMaturity(String sessionId, Map<String, Map> responsecache_onsessionId);

}
