package com.qc.Interceptors;

import java.util.Map;

public interface InputReceipt 
{

	public String getInputReceipt(String sessionId, Map<String, Map> responsecache_onsessionId);

}
