package com.qc.Interceptors;

import java.util.Map;

public interface InputSurrender 
{

	public String getInputSurrender(String sessionId, Map<String, Map> responsecache_onsessionId);

}
