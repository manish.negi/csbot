package com.qc.Interceptors;

import java.util.Map;

public interface PremiumPaymentTerm {

	 public String getPremiumPaymentTerm(String sessionId, Map<String, Map> responsecache_onsessionId);

}
