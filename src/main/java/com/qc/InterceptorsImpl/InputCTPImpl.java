package com.qc.InterceptorsImpl;

import java.util.Map;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.qc.Interceptors.InputCTP;
@Service
public class InputCTPImpl implements InputCTP 
{
	String speech="";
	//String sessionId = "";
	public ResourceBundle resProp = ResourceBundle.getBundle("errorMessages");
	private static Logger logger = LogManager.getLogger(PremiumPaymentTermImpl.class);
	@Override
	public String getInputCTP(String sessionId, Map<String, Map> responsecache_onsessionId) {
		logger.info("CASE :: input.CTP :: START");

		System.out.println("Action Invoded:- input.CTP");
		String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
		String cachevalidOTP = responsecache_onsessionId.get(sessionId).get("ValidOTP") + "";
		Map data = (Map) responsecache_onsessionId.get(sessionId).get("PolData");
		if ("".equalsIgnoreCase(cachePolicyNo) || cachePolicyNo == null) {
			speech = resProp.getString("validPolicyMessage");
		} else if ("".equalsIgnoreCase(cachevalidOTP) || cachevalidOTP == null) {
			speech = resProp.getString("validateOTP").concat(cachePolicyNo);

		} else {
			if (data.get("CTP") == null) {
				speech = ((Map) data.get("ErrorMessage")).get("Message").toString();
			} else {

				Map ctp = (Map) data.get("CTP");
				speech = ctp.get("Message").toString();
			}
		}
		logger.info("CASE :: input.CTP :: END");
		return speech;
	}
}
