package com.qc.InterceptorsImpl;

import java.util.Map;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qc.Interceptors.InputFund;
import com.qc.api.service.PolicyDetail_Service_FundValue;

@Service
public class InputFundImpl implements InputFund 
{
	@Autowired
	private PolicyDetail_Service_FundValue policyDetail_Service_FundValue;
	//String sessionId = "";
	String speech="";
	public ResourceBundle resProp = ResourceBundle.getBundle("errorMessages");
	private Logger logger = LogManager.getLogger(InputReceiptImpl.class);
	@Override
	public String getInputFund(String sessionId, Map<String, Map> responsecache_onsessionId) {
		System.out.println("Action Invoded:- input.Fund");
		String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
		String cachevalidOTP = responsecache_onsessionId.get(sessionId).get("ValidOTP") + "";
		Map data = (Map) responsecache_onsessionId.get(sessionId).get("PolData");
		if ("".equalsIgnoreCase(cachePolicyNo) || cachePolicyNo == null) {
			speech = resProp.getString("validPolicyMessage");
		} else if ("".equalsIgnoreCase(cachevalidOTP) || cachevalidOTP == null) {
			speech = resProp.getString("validateOTP").concat(cachePolicyNo);
		} else {
			if (data.get("FV") == null) {
				speech = ((Map) data.get("ErrorMessage")).get("Message").toString();
			} else {
				Map fv = (Map) data.get("FV");
				if (fv.get("fundValAsonDate") != null) {

					if (fv.get("FVErrorMessage") != null)
						speech = fv.get("FVErrorMessage").toString();
					else {
						String fvdata = fv.get("fundValAsonDate").toString();
						speech = fv.get("Message").toString() + " "
								+ Math.round(Double.parseDouble(fvdata) * 100.0) / 100.0;
						String mir_dv_eff_dt = policyDetail_Service_FundValue.policyDetilfunValue(cachePolicyNo);
						System.out.println("Date retrun from backend ::" + mir_dv_eff_dt);
					}
				} else {
					speech = fv.get("Message").toString();
				}
			}
		}
		return speech;
	}
}
