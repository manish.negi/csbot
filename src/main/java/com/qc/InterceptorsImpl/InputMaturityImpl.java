package com.qc.InterceptorsImpl;

import java.util.Map;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.qc.Interceptors.InputMaturity;

@Service
public class InputMaturityImpl implements InputMaturity 
{

	//String sessionId = "";
	String speech="";
	public ResourceBundle resProp = ResourceBundle.getBundle("errorMessages");
	private Logger logger = LogManager.getLogger(InputReceiptImpl.class);
	@Override
	public String getInputMaturity(String sessionId, Map<String, Map> responsecache_onsessionId) {
		String cashdata="";
		try
		{
			cashdata=((Map)responsecache_onsessionId.get(sessionId).get("OverAllMaturityCashData")).get("maturityMessage")+"";
		}
		catch(Exception ex)
		{
			System.out.println("Exception while extracting data from map :: "+ex);
		}
		if(cashdata==null || "".equalsIgnoreCase(cashdata))
		{
			cashdata=resProp.getString("validateOTP");
		}
		speech = cashdata;
		return speech;
	}
}
