package com.qc.InterceptorsImpl;

import java.util.Map;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qc.Interceptors.InputPolicyPack;
import com.qc.api.service.PolicyPack_Handler_Service;

@Service
public class InputPolicyPackImpl implements InputPolicyPack
{
	@Autowired
	PolicyPack_Handler_Service policyPack_Handler_Service;
	
	String speech="";
	//String sessionId = "";
	public ResourceBundle resProp = ResourceBundle.getBundle("errorMessages");
	private static Logger logger = LogManager.getLogger(PremiumPaymentTermImpl.class);
	@Override
	public String getInputPolicyPock(String sessionId, Map<String, Map> responsecache_onsessionId) {
		System.out.println("Action Invoded:- Input.PolicyPack");
		String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
		String cachevalidOTP = responsecache_onsessionId.get(sessionId).get("ValidOTP") + "";
		if ("".equalsIgnoreCase(cachePolicyNo) || cachePolicyNo == null) {
			speech = resProp.getString("validPolicyMessage");
		} else if ("".equalsIgnoreCase(cachevalidOTP) || cachevalidOTP == null) {
			speech = resProp.getString("validateOTP").concat(cachePolicyNo);
		} else {
			speech = policyPack_Handler_Service.getPolicyPackService(cachePolicyNo).get("Message");
			speech = speech + "\n Is there anything else I can assist you with?";
		}
		return speech;
	}
}
