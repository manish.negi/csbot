package com.qc.InterceptorsImpl;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qc.Interceptors.InputReceipt;
import com.qc.api.service.MliDoc_Handler_Service;

@Service
public class InputReceiptImpl implements InputReceipt 
{
	@Autowired
	private MliDoc_Handler_Service mliDoc_Handler_Service;
	//String sessionId = "";
	String speech="";
	public ResourceBundle resProp = ResourceBundle.getBundle("errorMessages");
	private Logger logger = LogManager.getLogger(InputReceiptImpl.class);
	@Override
	public String getInputReceipt(String sessionId, Map<String, Map> responsecache_onsessionId) {
		System.out.println("Action Invoded:- input.Receipt");
		String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
		String cachevalidOTP = responsecache_onsessionId.get(sessionId).get("ValidOTP") + "";
		String cashlastPremPmtDt = responsecache_onsessionId.get(sessionId).get("lastPremPmtDt") + "";
		// cashlastPremPmtDt="";
		Map data = (Map) responsecache_onsessionId.get(sessionId).get("PolData");

		if ("".equalsIgnoreCase(cachePolicyNo) || cachePolicyNo == null) {
			speech = resProp.getString("validPolicyMessage");
		} else if ("".equalsIgnoreCase(cachevalidOTP) || cachevalidOTP == null) {
			speech = resProp.getString("validateOTP").concat(cachePolicyNo);
		} else {
			speech = mliDoc_Handler_Service.getMliDocService(cachePolicyNo, cashlastPremPmtDt)
					.get("Message");
		}
		return speech;
	}
}
