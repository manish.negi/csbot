package com.qc.InterceptorsImpl;

import java.util.Map;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qc.Interceptors.InputSurrender;
import com.qc.api.service.PolicyDetail_Handler_Service;
@Service
public class InputSurrenderImpl implements InputSurrender 
{
	@Autowired
	PolicyDetail_Handler_Service policyDetail_Handler_Service;
	
	//String sessionId = "";
	String speech="";
	public ResourceBundle resProp = ResourceBundle.getBundle("errorMessages");
	private Logger logger = LogManager.getLogger(InputSurrenderImpl.class);
	@Override
	public String getInputSurrender(String sessionId, Map<String, Map> responsecache_onsessionId) {
		System.out.println("Action Invoded:- input.Surrender");
		String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
		String cachevalidOTP = responsecache_onsessionId.get(sessionId).get("ValidOTP") + "";
		Map data = (Map) responsecache_onsessionId.get(sessionId).get("PolData");
		if ("".equalsIgnoreCase(cachePolicyNo) || cachePolicyNo == null) {
			speech = resProp.getString("validPolicyMessage");
		} else if ("".equalsIgnoreCase(cachevalidOTP) || cachevalidOTP == null) {
			speech = resProp.getString("validateOTP").concat(cachePolicyNo);
		} else {
			if (data.get("CSV") == null) {
				speech = policyDetail_Handler_Service.getPolicyDetails(data, cachePolicyNo).get("Message")
						.toString();
			} else {
				speech = ((Map) data.get("CSV")).get("Message").toString();
			}
		}
		return speech;
	}
}
