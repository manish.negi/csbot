package com.qc.InterceptorsImpl;

import org.springframework.stereotype.Service;

import com.qc.Interceptors.LifeStage;

@Service
public class LifeStageImpl implements LifeStage
{
	String speech="";
	@Override
	public String getLifeStage(String LifeStage)
	{
		if (LifeStage != null && !"".equalsIgnoreCase(LifeStage))
		{
			if("Single and Independent".equalsIgnoreCase(LifeStage))
			{
				speech="Thank you. That means you're not just independent, but also financially responsible. Please tell me what brings you to Max Life:";
			}
			else if("Newly Married".equalsIgnoreCase(LifeStage))
			{
				speech = "Congratulations! I wish you a very happy married life. Please tell me what brings you to Max Life:";
			}
			else if ("Married with young  children".equalsIgnoreCase(LifeStage) || "Married with teenage children".equalsIgnoreCase(LifeStage))
			{
				speech="Thank you. I am sure your kids don't fail to surprise you each day and you being here sure proves that you're a responsible parent. Please tell me what brings you to Max Life:";
			}
			else if ("Nearing Retirement".equalsIgnoreCase(LifeStage))
			{
				speech="Thank you. You'll soon have lots of time at hand. So let's make sure your finances support a great lifestyle:";
			}
		}
		else
		{
			speech="There is some communication glitch on this life stage, please try after sometime.";
		}
		
		return speech;
	}

}
