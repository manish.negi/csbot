package com.qc.InterceptorsImpl;

import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qc.Interceptors.LocateusPinCode;
import com.qc.api.service.AddressDetail;
import com.qc.common.RegexMatcher;

@Service
public class LocateusPinCodeImpl implements LocateusPinCode 
{
	@Autowired
	RegexMatcher regexMatcher;
	@Autowired
	AddressDetail addressDetail;
	String speech="";
	String sessionId = "";
	String status="";
	public ResourceBundle resProp = ResourceBundle.getBundle("errorMessages");
	private static Logger logger = LogManager.getLogger(PremiumPaymentTermImpl.class);
	@Override
	public String getLocateUsPinCode(String pinCode) {
		try {
			System.out.println("Action Invoded:- PinCode");
			RegexMatcher reg = new RegexMatcher();
			status = reg.regexPatternStatus(pinCode);
		} catch (Exception ex) {
			System.out.println("Exception occoured in Locateuspincode statement");
		}
		return status;
	}
}
