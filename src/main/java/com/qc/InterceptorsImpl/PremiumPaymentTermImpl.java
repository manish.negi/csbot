package com.qc.InterceptorsImpl;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.qc.Interceptors.PremiumPaymentTerm;
import com.qc.common.Commons;
import com.qc.common.CustomizeDate;

@Service
public class PremiumPaymentTermImpl implements PremiumPaymentTerm 
{
	//String sessionId = "";
	String speech="";
	public ResourceBundle resProp = ResourceBundle.getBundle("errorMessages");
	private static Logger logger = LogManager.getLogger(PremiumPaymentTermImpl.class);
	@Override
	public String getPremiumPaymentTerm(String sessionId, Map<String, Map> responsecache_onsessionId) {
		if (responsecache_onsessionId.containsKey(sessionId)) {
			System.out.println("Action Invoded:- Policy.PremiumPaymentTerm");
			String premChngeDtCd = "";
			String premChngAgeDur = "";
			String cvgMatXpryDt = "";
			String cvgIssEffDt = "";
			String cvgPlanIdCd = "";
			String origPlanIdCd = "";
			String maturitycashdata="";
			String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
			Map<String, Object> cashdata = (Map) responsecache_onsessionId.get(sessionId).get("OverAllMaturityCashData");
			try
			{
				maturitycashdata=cashdata.get("MaturityCashData")+"";
			}
			catch(Exception ex){
				System.out.println("exception while extracting data from Map :: "+ex );
			}
			if(maturitycashdata==null || "".equalsIgnoreCase(maturitycashdata)){
				maturitycashdata=resProp.getString("validateOTP");
			}
			Map<String, Object> resultData = Commons.getGsonData(maturitycashdata);
			String policyNumber = ((Map) ((Map) ((Map) resultData.get("response")).get("responseData"))
					.get("BasicDetails")).get("policyNum").toString();
			String policyStatusCd = ((Map) ((Map) ((Map) resultData.get("response")).get("responseData"))
					.get("BasicDetails")).get("policyStatusCd").toString();
			String policyStatusDesc = ((Map) ((Map) ((Map) resultData.get("response")).get("responseData"))
					.get("BasicDetails")).get("policyStatusDesc").toString();
			String polDueDate = ((Map) ((Map) ((Map) resultData.get("response")).get("responseData"))
					.get("BasicDetails")).get("polDueDate").toString();
			String billingFreqCd = ((Map) ((Map) ((Map) resultData.get("response")).get("responseData"))
					.get("BasicDetails")).get("billingFreqCd").toString();
			String billingFreqDesc = ((Map) ((Map) ((Map) resultData.get("response")).get("responseData"))
					.get("BasicDetails")).get("billingFreqDesc").toString();
			List cvgDetails = (List) ((Map) ((Map) resultData.get("response")).get("responseData"))
					.get("coverageDetailsArray");
			if (cvgDetails != null && !cvgDetails.isEmpty()) {
				for (int i = 0; i < cvgDetails.size(); i++) {
					String cvgNum = ((Map) cvgDetails.get(i)).get("cvgNum") + "";
					if ("01".equals(cvgNum)) {
						premChngeDtCd = ((Map) cvgDetails.get(i)).get("premChngeDtCd") + "";
						premChngAgeDur = ((Map) cvgDetails.get(i)).get("premChngAgeDur") + "";
						cvgMatXpryDt = ((Map) cvgDetails.get(i)).get("cvgMatXpryDt") + "";
						cvgIssEffDt = ((Map) cvgDetails.get(i)).get("cvgIssEffDt") + "";
						cvgPlanIdCd = ((Map) cvgDetails.get(i)).get("cvgPlanIdCd") + "";
						origPlanIdCd = ((Map) cvgDetails.get(i)).get("origPlanIdCd") + "";
					}
				}
			}
			if ("1".equalsIgnoreCase(policyStatusCd)) {
				CustomizeDate custdate = new CustomizeDate();
				if (!"A".equalsIgnoreCase(premChngeDtCd)) {

					int i=0;
					try {
						i = custdate.comparetwoDates(polDueDate, cvgMatXpryDt);
					} catch (ParseException e) {
						logger.info("Exception Occoured while parsing date"+PremiumPaymentTermImpl.class);
					}
					if (i == 2) {
						speech = resProp.getString("maturity21");
					} else {
						int month = custdate.getMonth(cvgMatXpryDt, polDueDate);
						String customizeDate = custdate.subtractMonth(cvgMatXpryDt, billingFreqCd);
						int billingFreqcd = Integer.parseInt(billingFreqCd);
						int PremDueCount1 = month / billingFreqcd;
						speech = resProp.getString("maturity22") + " " + cachePolicyNo + " "
								+ resProp.getString("maturity8") + " "
								+ Commons.convertDateFormat(customizeDate) + " "
								+ resProp.getString("premium25") + " " + PremDueCount1
								+ " " + billingFreqDesc + " " + resProp.getString("premium26");
					}
				} else {
					String premChngAgeDurYear = custdate.addYear(cvgIssEffDt, premChngAgeDur);
					int ir=0;
					try {
						ir = custdate.comparetwoDates(polDueDate, premChngAgeDurYear);
					} catch (ParseException e) {
						logger.info("Exception Occoured while parsing date"+PremiumPaymentTermImpl.class);
					}
					if (ir == 2) {
						try {

							speech = "Thank you. All due premiums have already been paid for the policy.";
						} catch (Exception ex) {
							System.out.println(ex);
						}
					} else {
						String getYear = custdate.addYear(cvgIssEffDt, premChngAgeDur);
						String customizeDate = custdate.subtractMonth(getYear, billingFreqCd);
						int month = custdate.getMonth(getYear, polDueDate);
						int billingFreqcd = Integer.parseInt(billingFreqCd);
						int PremDueCount2 = month / billingFreqcd;
						speech = resProp.getString("maturity22") + " " + cachePolicyNo + " "
								+ resProp.getString("maturity8") + " "
								+ Commons.convertDateFormat(customizeDate) + " "
								+ resProp.getString("premium25") + " " + PremDueCount2 + " " + billingFreqDesc
								+ " " + resProp.getString("premium26");
					}
				}
			} else {
				if ("3".equalsIgnoreCase(policyStatusCd) || "4".equalsIgnoreCase(policyStatusCd)) {
					if (cvgPlanIdCd.equalsIgnoreCase(origPlanIdCd)) {
						speech = "Thank You. All due premiums have already been paid for the policy. ";
					} else {
						speech = "The premium payment end date for your policy " + policyNumber
								+ " can not be provided as it is in " + policyStatusDesc + " status. "
								+ "Please call our customer care on 1-860-120-5577 to get more details on the same.";
					}
				} else {
					speech = "The premium payment end date for your policy " + policyNumber
							+ " can not be provided as it is in " + policyStatusDesc + " status. "
							+ "Please call our customer care on 1-860-120-5577 to get more details on the same.";
				}
			}
		}
		return speech;
	}
}
