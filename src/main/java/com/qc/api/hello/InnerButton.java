package com.qc.api.hello;

import java.io.Serializable;

public class InnerButton implements Serializable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6750098815506417774L;
	String text;
	String postback;
	String link;
	
	public InnerButton() {
		super();
	}

	public InnerButton(String text, String postback, String link) {
		super();
		this.text = text;
		this.postback = postback;
		this.link = link;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getPostback() {
		return postback;
	}

	public void setPostback(String postback) {
		this.postback = postback;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}


}
