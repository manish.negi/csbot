package com.qc.api.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.qc.common.AddressDetailService;
@Component
public class AddressDetail 
{
	private static Logger logger = LogManager.getLogger(AddressDetail.class);
	public String getAddress(String pinCode)
	{
		String response="";
		Map<String,String> map = new HashMap<String,String>();
		try{
			AddressDetailService addressdetail = new AddressDetailService();
			String result=addressdetail.AddressDetail(pinCode);
			StringBuilder builder = new StringBuilder();
			JSONArray jsonArr = new JSONArray(result);
			for (int i = 0; i < jsonArr.length(); i++)
			{
				builder.append("Nearest Max Life office, based on the PIN code entered is as follows:\n\n");
				JSONObject jsonObj = jsonArr.getJSONObject(i);
				for(int k=1; k<=1; k++)
				{
					String address="";
					String pincode="";
					String telephone_no="";
					String valuek=String.valueOf(k);
					address=jsonObj.get("address"+valuek)+"";
					pincode=jsonObj.get("pincode"+valuek)+"";
					telephone_no=jsonObj.get("telephone_no"+valuek)+"";
					if(address!="" && address!=null)
					{
						builder.append("Address:- "+address+"\n Phone No:- "+telephone_no+"\n\n");
					}
				}
			}
			response=builder.toString();
		}catch(Exception ex)
		{
			logger.info("Exception Occoured While Parsing JsonResponse");
			response="";
		}
		return response;
	}
}
