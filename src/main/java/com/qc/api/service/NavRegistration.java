package com.qc.api.service;

import java.util.Map;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.qc.common.Commons;
import com.qc.common.Httpurl_Connection;
import com.qc.common.NavApi_Calling;

@Component
public class NavRegistration {
	
	public static ResourceBundle res = ResourceBundle.getBundle("errorMessages");
	private static Logger logger = LogManager.getLogger(NavRegistration.class);
	
	@Autowired
	NavApi_Calling navApiCalling;

	public String getNavRegistartion(String policyNo)
	{
		String result="";
		try
		{
			logger.info("CameInside Method :: getNavRegistartion :: START ");
			Httpurl_Connection connection = new Httpurl_Connection();
			String PolicyInfo = "PolicyInfo";
			String finaldate = "";
			
			String str = connection.httpConnection_response(policyNo, PolicyInfo, finaldate);
			logger.info("PolicyInfo360 API Response From Backend ::  " + str.toString());

			Map resultData = Commons.getGsonData(str);
			String soaStatusCode = ((Map) ((Map) resultData.get("response")).get("responseData")).get("soaStatusCode")
					.toString();
			if (soaStatusCode != null && !"".equalsIgnoreCase(soaStatusCode) && soaStatusCode.equalsIgnoreCase("200"))
			{
				String policyInsuranceTypeCd = ((Map) ((Map) ((Map) resultData.get("response")).get("responseData"))
						.get("BasicDetails")).get("policyInsuranceTypeCd").toString();
				
				String policyStatusDesc = ((Map) ((Map) ((Map) resultData.get("response")).get("responseData"))
						.get("BasicDetails")).get("policyStatusDesc").toString();
				
				String policyStatusCd = ((Map) ((Map) ((Map) resultData.get("response")).get("responseData"))
						.get("BasicDetails")).get("policyStatusCd").toString();
				

				String discontinuanceFund = ((Map) ((Map) ((Map) resultData.get("response")).get("responseData"))
						.get("BasicDetails")).get("discontinuanceFund").toString();
				

				String fundValAsonDate = ((Map) ((Map) ((Map) resultData.get("response")).get("responseData"))
						.get("PolicyMeasures")).get("fundValAsonDate").toString();
				
				if("N".equalsIgnoreCase(policyInsuranceTypeCd) || "D".equalsIgnoreCase(policyInsuranceTypeCd)||
						"F".equalsIgnoreCase(policyInsuranceTypeCd) || "C".equalsIgnoreCase(policyInsuranceTypeCd))
				{
					
					if(("1".equalsIgnoreCase(policyStatusCd) || "2".equalsIgnoreCase(policyStatusCd) ||
						"1A".equalsIgnoreCase(policyStatusCd) || "3".equalsIgnoreCase(policyStatusCd)||"4".equalsIgnoreCase(policyStatusCd))||
						 ("5".equalsIgnoreCase(policyStatusCd) && "".equalsIgnoreCase(discontinuanceFund)&&!"0".equalsIgnoreCase(fundValAsonDate)))
							 {
						
						String navRegistrationStatus = navApiCalling.navFundDetail(policyNo);
						logger.info("get Fund Details API Response From Backend ::  " + navRegistrationStatus.toString());
						
						if (navRegistrationStatus.contains("There is some communication glitch!")) {
							result = "There is some communication glitch! Please try again after some time. Error Code -MAX00PLD";
						}
						else
						{
						if("Y".equalsIgnoreCase(navRegistrationStatus))
						{
							result="I am registering you for NAV alerts. Shall I go ahead & update the policy records? Please note that once registered, you will receive daily alerts.";
						}
						else
						{
							result="As per our records, your policy is already registered for NAV alerts.<Br/>" + 
									"Is there anything else I can assist you with? ";
						}
						}
							 }
					else
					{
						result="Sorry, NAV alerts cannot be activated on your policy as the status of your policy is <"+policyStatusDesc+">.<Br/>" + 
								"Is there anything else I can assist you with? ";
					}
				}
				else
				{
					result="Sorry, NAV is not applicable for this product.<Br/>" + 
							"Is there anything else I can assist you with?";
				}

			}
			else
			{
				result="Getting error : while calling backend service:: soaStatusCode is :-"+soaStatusCode;
			}
			
		}catch(Exception ex)
		{
			logger.info("Exception occured"+ex);
		}
		return result;
	}
	
	public String navUpadte(String policyNo,String transactionType)
	{
		String result="";
		result=navApiCalling.navUpdateApi(policyNo,transactionType);
		return result;
	}
	
}
