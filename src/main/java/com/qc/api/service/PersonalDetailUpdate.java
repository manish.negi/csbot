package com.qc.api.service;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;
import javax.net.ssl.HttpsURLConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import com.qc.common.Commons;

@Component
public class PersonalDetailUpdate {

	public static ResourceBundle res = ResourceBundle.getBundle("errorMessages");
	private static Logger logger = LogManager.getLogger(PersonalDetailUpdate.class);
	public String getCustomerInfo(String policyNo, String intent, String flag)
	{
		String result="";
		String currentMobileNumber="";
		String currentEmail="";
		String  DevMode = "N";
		StringBuilder resultData = new StringBuilder();
		String output = new String();
		HttpURLConnection conn = null;

		try
		{
			String serviceurl = res.getString("getcustomer_info_url");
			System.out.println("SERVICE URL :-"+serviceurl);
			URL url = new URL(serviceurl);
			if(DevMode!=null && !"".equalsIgnoreCase(DevMode) && "Y".equalsIgnoreCase(DevMode))
			{
				Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("cachecluster.maxlifeinsurance.com", 3128));
				conn = (HttpURLConnection) url.openConnection(proxy);
			}else{
				conn = (HttpURLConnection) url.openConnection();
			}
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			StringBuilder sb=new StringBuilder();
			sb.append("{ ");
			sb.append("\"request\": { ");
			sb.append("\"header\": { ");
			sb.append("\"soaMsgVersion\": \"1.0\",");
			sb.append("\"soaCorrelationId\": \"225163708\",");
			sb.append("\"soaAppId\": \"CSBOT\"");
			sb.append("}, ");
			sb.append("\"requestData\": { ");
			sb.append("\"policyNo\": \""+policyNo+"\" ");
			sb.append("}");
			sb.append("}");
			sb.append("}");
			System.out.println("START External API Call : getcustomerinfo");
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(sb.toString());
			writer.flush();
			try {writer.close(); } catch (Exception e1) {}
			int apiResponseCode = conn.getResponseCode();
			System.out.println("API Response Code : getcustomerinfo"+ apiResponseCode);
			if(apiResponseCode == 200)
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				while ((output = br.readLine()) != null) 
				{
					resultData.append(output);
				}
				conn.disconnect();
				br.close();
				System.out.println("END External API Call : getcustomerinfo");
			}
			else
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
				while ((output = br.readLine()) != null) {
					resultData.append(output);
				}
				conn.disconnect();
				br.close();
			}
			if(apiResponseCode == 200)
			{
				Map personaldetailupdate = new  Commons().getGsonData(resultData.toString());

				Map responseData = (Map)((Map)personaldetailupdate.get("response")).get("responseData");
				if("200".equalsIgnoreCase(responseData.get("soaStatusCode")+""))
				{
					if(intent.equalsIgnoreCase("MobileNo"))
					{
						JSONObject jsonobj= new JSONObject(responseData);
						try
						{
							currentMobileNumber = jsonobj.getJSONArray("mobileNumber").getJSONObject(0).getString("mobileNo")+"";
						}catch(Exception e)
						{
							System.out.println(e);
						}
						System.out.println(currentMobileNumber);
						if((currentMobileNumber)!=null||!"".equalsIgnoreCase(currentMobileNumber))
						{
							if(currentMobileNumber.equalsIgnoreCase(flag))

							{
								result="I just checked our records, your mobile number is already updated in policy records." + 
										"\n" + 
										"Is there anything else I can assist you with? ";
							}
							else
							{
								result = "I'm now updating your mobile number from " + currentMobileNumber + " to "
										+ flag
										+ ". Shall I go ahead & update the policy records?";
							}
						}
					}
					else
					{
						JSONObject jsonobj= new JSONObject(responseData);
						System.out.println("jsonobj"+jsonobj);
						try
						{
							currentEmail = jsonobj.getJSONArray("emailIds").getJSONObject(0).getString("emailId")+"";
						}catch(Exception e)
						{
							System.out.println(e);
						}
						System.out.println("currentEmail"+currentEmail);
						if((currentEmail)!=null||!"".equalsIgnoreCase(currentEmail))
						{
							if(currentEmail.equalsIgnoreCase(flag))
							{
								result="As per our records, this email id is already updated in policy records.\n" + 
										"Is there anything else I can assist you with? ";
							}
							else
							{
								result = "I'm now updating your email id from " + currentEmail + " to "
										+ flag
										+ ". Shall I go ahead & update the policy records?";
							}

						}
					}
				}

			}
			else
			{
				return "There is some communication glitch! Please try again after some time. Error Code -MAX00PLD";
			}
		}catch(Exception ex)
		{
			return "There is some communication glitch! Please try again after some time. Error Code -EXMAX00PLD";
		}
		return result;

	}
	public String updateMobile(String policyNo, String newMobileNumber)
	{
		String  DevMode = "N";
		StringBuilder resultData = new StringBuilder();
		String output = new String();
		HttpURLConnection conn = null;
		try
		{
			String serviceurl = res.getString("mobile_update_url");
			System.out.println("SERVICE URL :-"+serviceurl);
			URL url = new URL(serviceurl);
			if(DevMode!=null && !"".equalsIgnoreCase(DevMode) && "Y".equalsIgnoreCase(DevMode))
			{
				Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("cachecluster.maxlifeinsurance.com", 3128));
				conn = (HttpURLConnection) url.openConnection(proxy);
			}else{
				conn = (HttpURLConnection) url.openConnection();
			}
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");

			StringBuilder sb=new StringBuilder();
			sb.append("{ ");
			sb.append("\"request\": { ");
			sb.append("\"header\": { ");
			sb.append("\"soaMsgVersion\": \"1.0\",");
			sb.append("\"soaCorrelationId\": \"0024578965\",");
			sb.append("\"soaAppId\": \"CSBOT\"");
			sb.append("}, ");
			sb.append("\"requestData\": { ");
			sb.append("\"policyNo\": \""+policyNo+"\",");
			sb.append("\"mobileNo\": \""+newMobileNumber+"\" ");
			sb.append("}");
			sb.append("}");
			sb.append("}");

			System.out.println("START External API Call : updatemobile");
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(sb.toString());
			writer.flush();
			try {writer.close(); } catch (Exception e1) {}
			int apiResponseCode = conn.getResponseCode();
			System.out.println("API Response Code : updatemobile"+ apiResponseCode);
			if(apiResponseCode == 200)
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				while ((output = br.readLine()) != null) 
				{
					resultData.append(output);
				}
				conn.disconnect();
				br.close();
				System.out.println("END External API Call : updatemobile");
			}
			else
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
				while ((output = br.readLine()) != null) {
					resultData.append(output);
				}
				conn.disconnect();
				br.close();
			}
			if(apiResponseCode == 200)
			{
				Map personaldetailupdate = new  Commons().getGsonData(resultData.toString());
				Map responseData = (Map)((Map)personaldetailupdate.get("response")).get("responseData");
				if("200".equalsIgnoreCase(responseData.get("soastatusCode")+"")
						&& "Success".equalsIgnoreCase(responseData.get("soaMessage")+""))
				{
					return "Thank you for updating your mobile number, it will get updated in our records within next 1 hour."
							+" Is there anything else I can assist you with?";
				}
				else
				{
					return "Sorry!! We are unable to update your policy records at this time. Please try again later."
							+" Is there anything else I can assist you with?";

				}
			}
			else
			{
				return "There is some communication glitch! Please try again after some time. Error Code -MAX00PLD";
			}
		}catch(Exception ex)
		{
			return "There is some communication glitch! Please try again after some time. Error Code -EXMAX00PLD";
		}


	}

	public String updateEmail(String policyNo, String emailId)
	{
		String  DevMode = "N";
		StringBuilder resultData = new StringBuilder();
		String output = new String();
		HttpURLConnection conn = null;
		try
		{
			String serviceurl = res.getString("email_update_url");
			System.out.println("SERVICE URL :-"+serviceurl);
			URL url = new URL(serviceurl);
			if(DevMode!=null && !"".equalsIgnoreCase(DevMode) && "Y".equalsIgnoreCase(DevMode))
			{
				Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("cachecluster.maxlifeinsurance.com", 3128));
				conn = (HttpURLConnection) url.openConnection(proxy);
			}else{
				conn = (HttpURLConnection) url.openConnection();
			}
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");

			StringBuilder sb=new StringBuilder();
			sb.append("{ ");
			sb.append("\"request\": { ");
			sb.append("\"header\": { ");
			sb.append("\"soaMsgVersion\": \"1.0\",");
			sb.append("\"soaCorrelationId\": \"0024578965\",");
			sb.append("\"soaAppId\": \"CSBOT\"");
			sb.append("}, ");
			sb.append("\"requestData\": { ");
			sb.append("\"policyNo\": \""+policyNo+"\",");
			sb.append("\"emailId\": \""+emailId+"\" ");
			sb.append("}");
			sb.append("}");
			sb.append("}");
			System.out.println("START External API Call : updatemobile");
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(sb.toString());
			writer.flush();
			try {writer.close(); } catch (Exception e1) {}
			int apiResponseCode = conn.getResponseCode();
			System.out.println("API Response Code : updatemobile"+ apiResponseCode);
			if(apiResponseCode == 200)
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				while ((output = br.readLine()) != null) 
				{
					resultData.append(output);
				}
				conn.disconnect();
				br.close();
				System.out.println("END External API Call : updatemobile");
			}
			else
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
				while ((output = br.readLine()) != null) {
					resultData.append(output);
				}
				conn.disconnect();
				br.close();
			}
			if(apiResponseCode == 200)
			{
				Map personaldetailupdate = new  Commons().getGsonData(resultData.toString());
				Map responseData = (Map)((Map)personaldetailupdate.get("response")).get("responseData");
				if("200".equalsIgnoreCase(responseData.get("soastatusCode")+"")
						&& "Success".equalsIgnoreCase(responseData.get("soaMessage")+""))
				{
					return "Thank you for updating your email ID, it will get updated in our records within next 1 hour."
							+ " Is there anything else I can assist you with?";
				}
				else
				{
					return "Sorry!! We are unable to update your policy records at this time. Please try again later."
							+ " Is there anything else I can assist you with?";

				}
			}
			else
			{
				return "There is some communication glitch! Please try again after some time. Error Code -MAX00PLD";
			}
		}catch(Exception ex)
		{
			return "There is some communication glitch! Please try again after some time. Error Code -EXMAX00PLD";
		}
	}
}
