package com.qc.api.service;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import com.qc.common.Commons;
import com.qc.common.Httpurl_Connection;

@Component
public class PolicyDetail_Service_FundValue 
{
	private static Logger logger = LogManager.getLogger(PolicyDetail_Service_FundValue.class);

	public String policyDetilfunValue(String policyNumber) 
	{
		String policyDetail ="PolicyDetail";
		String finaldate="";
		Httpurl_Connection connection = new Httpurl_Connection();
		String result=connection.httpConnection_response(policyNumber, policyDetail, finaldate);
		String mir_dv_eff_dt = "";
		try 
		{
			Map resultData = Commons.getGsonData(result);
			mir_dv_eff_dt = ((Map) ((Map) ((Map) resultData.get("response")).get("responseData"))
					.get("cashSurrenderValue")).get("MIR-DV-EFF-DT").toString();
		} catch (Exception ex)
		{
			logger.info("Exception Occured"+ ex);
			logger.info("Something went wrong in policyDetailfunValue Method::");
		}
		return Commons.convertDateFormat(mir_dv_eff_dt);
	}


}

