package com.qc.api.service;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.qc.common.Commons;
import com.qc.common.XTrustProviderSSL;

@Component
public class PolicyLoanDetail 
{
	public String policyLoanDetail(String PolicyNo, String customerDOB, String flag)
	{
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		Date date = new Date();
		 UUID id = UUID.randomUUID();
		 ResponseEntity<String> response =null;
		try
		{
			String soaurl="https://gatewayuat.maxlifeinsurance.com/apimgm/sb/soa/salesandcustomerservice/policydetails/policyandloandetails/v1";
			XTrustProviderSSL.trustHttps();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("client_id", "27181c84-7120-4810-862d-b443f3b3bae8");
			headers.set("client_secret", "hU2lA8yS8jH5oF1tR0iY4hN2kV3eI5gV7iP1wI6mW5dP2gU7fT");
			StringBuilder sb=new StringBuilder();
			sb.append("{ ");
			sb.append("\"request\": { ");
			sb.append("\"header\": { ");
			sb.append("\"soaMsgVersion\": \"1.0\",");
			sb.append("\"soaCorrelationId\": \""+id+"\",");
			//sb.append("\"soaCorrelationId\": \"123456\",");
			sb.append("\"soaAppId\": \"CSBOT\"");
			sb.append("}, ");
			sb.append("\"requestData\": { ");
			sb.append("\"policyID\": \""+PolicyNo+"\",");
			sb.append("\"dob\": \""+customerDOB+"\" ");
			sb.append("}");
			sb.append("}");
			sb.append("}");
			HttpEntity<String> entity=new HttpEntity<String>(sb.toString(),	headers);
			RestTemplate restTemplate=new RestTemplate();
			restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
			try{
				response = restTemplate.exchange(soaurl, HttpMethod.POST, entity,String.class);
			}catch(Exception ex)
			{
				return "No policy found matching the entered details, Please enter policy holder's date of birth in DD-MM-YYYY format.";
			}
			
			Map policyloanData = new  Commons().getGsonData(response.getBody());
			Map responseData = (Map)((Map)policyloanData.get("response")).get("responseData");
			if(response.getStatusCodeValue() == 200)
			{
				
				if("200".equalsIgnoreCase(responseData.get("soaStatusCode")+"") 
						&& "Success".equalsIgnoreCase(responseData.get("soaMessage")+""))
				{
					if("loanegigibilitydetails".equalsIgnoreCase(flag))
					{
						if("Y".equalsIgnoreCase(responseData.get("isEligibleForLoan")+""))
						{
							if(Double.parseDouble(responseData.get("loanAmount")+"")!= 0)
							{
								return "Happy to inform you that you can avail loan on your policy up to Rs. "+responseData.get("eligibleLoanAmount")+""+" ,"
										+ "current rate of  interest is 9.9%.  Current outstanding loan amount on your policy is "
										+ " Rs."+responseData.get("loanAmount")+""+" <a href='https://goo.gl/SfqNxA' target='_blank'> click here </a> "
										+ " to download the loan request form. \n "
										+ " Is there anything else I can help you with? (Yes/No)";
								
							}
							else
							{
								return " Happy to inform you that you can avail loan on your policy up to Rs. "+responseData.get("eligibleLoanAmount")+""+" ,"
										+ " current rate of  interest is 9.9%.  <a href='https://goo.gl/SfqNxA' target='_blank'> click here </a> "
										+ "to download the loan request form.\n "
										+ " Is there anything else I can help you with? (Yes/No)";
							}
						}
						else
						{
							return "I see from the records, your policy is not eligible for availing loan currently."
									+ " Is there anything else I can help you with? (Yes/No)";
						}
					}
					else
					{
						if(Double.parseDouble(responseData.get("loanAmount")+"")!= 0)
						{
							return " As on "+format.format(date)+", outstanding loan amount on your Max Life policy "+ PolicyNo+" is "
									+ " Rs."+responseData.get("loanAmount")+". The interest of Rs."+responseData.get("loanAmount")+ " is due. "
									+ " <a href='bit.ly/1Wis1l9' target='_blank'> click here </a> to make the payment or please walk into Max Life branch to pay at the earliest. "
									+ " Is there anything else I can help you with? (Yes/No)";
						}
						else
						{
							return " As on"+format.format(date)+" there is no outstanding loan on your Max Life policy."
									+ " Is there anything else I can help you with? (Yes/No)";
						}
					}
				}
			}
			else 
			{
				return "There is some communication glitch! Please try again after some time. Error Code -MAX00PLD";
			}
		}catch(Exception ex)
		{
			return "There is some communication glitch! Please try again after some time. Error Code -EXMAX00PLD";
		}
		return "";
	}
}



