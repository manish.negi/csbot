package com.qc.api.service;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

//import org.apache.axis.client.Call;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import com.qc.common.Commons;
import com.qc.common.Httpurl_Connection;

@Component
public class PolicyPack_Handler_Service {
	private static Logger logger = LogManager.getLogger(PolicyPack_Handler_Service.class);
	public static ResourceBundle res = ResourceBundle.getBundle("errorMessages");

	public Map<String, String> getPolicyPackService(String policyNo) {
		logger.info("Came Inside" + "getPolicyPackService :: Method :: STARTS");
		String finaldate = "";
		String mliDoc = "PolicyPack";
		String result = "";
		HashMap<String, String> returnMap = new HashMap();
		// For UAT we have comment this method becuase at uat it is not sent the mail
		// soa service is not responding correct
		// response this Call
		// new Thread(new Runnable()
		// {
		// public void run()
		// {
		logger.info("policy pack service calling start :-");
		Httpurl_Connection connection = new Httpurl_Connection();
		result = connection.httpConnection_response(policyNo, mliDoc, finaldate);
		// }
		// }).start();*/
		if ("200".equalsIgnoreCase(result)) {
			returnMap.put("Message", res.getString("PolicyPackServiceSuccess"));
		} else {
			try {

				Map resultData = Commons.getGsonData(result);
				String soaStatusCode = ((Map) ((Map) resultData.get("response")).get("responseData")).get("statusCode")
						.toString();
				String statusMessage = ((Map) ((Map) resultData.get("response")).get("responseData"))
						.get("statusMessage").toString();
				if (soaStatusCode != null && !"".equalsIgnoreCase(soaStatusCode)
						&& soaStatusCode.equalsIgnoreCase("200")) {
					/*
					 * String statusMessage = ((Map) ((Map)
					 * resultData.get("response")).get("responseData"))
					 * .get("statusMessage").toString();
					 */
					// String statusMessage ="Success";
					if (statusMessage.startsWith("Failure")) {
						returnMap.put("Message", res.getString("PolicyPackServiceFailure"));
					} else {
						returnMap.put("Message", res.getString("PolicyPackServiceSuccess"));
					}
				} else if (soaStatusCode.equalsIgnoreCase("500")) {
					if (statusMessage.contains("You have not registered your email ID with us")) {
						returnMap.put("Message", res.getString("PolicyPackServiceEmailNotFound1")
								+ res.getString("PolicyPackServiceEmailNotFound2"));
					}
				} else {
					returnMap.put("Message", res.getString("PolicyPackServiceFailure"));

				}

				logger.info("Came OutSide" + "getPolicyPackService :: Method :: END");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return returnMap;

	}
}
