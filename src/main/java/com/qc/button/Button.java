package com.qc.button;

import com.qc.api.hello.InnerData;

public interface Button 
{
	public InnerData getButtonsHI(String action);
	public InnerData getButtonsYesNo(String action);
	public InnerData getButtonsRenewalPaymentProcedure(String action);
	public InnerData getButtonsLoanInquiry(String action);
	public InnerData getButtonsUpdatePersonalDetails(String action);
	public InnerData getButtonsUpdateAadhaar_MobileEntered_EmailEntered(String action);
	public InnerData getPayOnline(String action);
	public InnerData getHelpfulSomeHelpful(String action);
	public InnerData getButtonFromDB(String result);
	public InnerData getButtonHello(String action);
	public InnerData getButtonBuyNow(String result, String url);
	//public InnerData getButtonOnlineOffline(String result, String BuyNowUrl, String KnowMoreUrl);
	public InnerData getButtonOnlineOffline(String result);
	
}
