package com.qc.buttonImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonValue;
import com.qc.api.hello.Facebook;
import com.qc.api.hello.InnerButton;
import com.qc.api.hello.InnerData;
import com.qc.button.Button;
import com.qc.common.AddressDetailService;

@Service
public class ButtonImpl implements Button 
{
	
	@Autowired
	AddressDetailService addressDetailService;
	
	List<InnerButton> innerbuttonlist = new ArrayList<InnerButton>();
	Facebook fb = new Facebook();
	InnerData innerData = new InnerData();
	InnerButton button = new InnerButton();
	InnerButton button2 = new InnerButton();
	InnerButton button3 = new InnerButton();
	InnerButton button4 = new InnerButton();
	InnerButton button5 = new InnerButton();
	InnerButton button6 = new InnerButton();
	InnerButton button7 = new InnerButton();
	InnerButton button8 = new InnerButton();
	InnerButton button9 = new InnerButton();
	InnerButton button10 = new InnerButton();
	InnerButton button11 = new InnerButton();
	InnerButton button12 = new InnerButton();
	InnerButton button13 = new InnerButton();
	InnerButton button14 = new InnerButton();
	@Override
	public InnerData getButtonsHI(String action) 
	{
		innerbuttonlist=new ArrayList<InnerButton>();
		innerData=new InnerData();
		
		button.setText("Premium Due");
		button.setPostback("premiumdue");
		button.setLink("");
		innerbuttonlist.add(button);

		button2.setText("Policy Term");
		button2.setPostback("maturity and term");
		button2.setLink("");
		innerbuttonlist.add(button2);

		button3.setText("Policy Pack");
		button3.setPostback("policypack");
		button3.setLink("");
		innerbuttonlist.add(button3);

		button4.setText("Maturity");
		button4.setPostback("maturity and term");
		button4.setLink("");
		innerbuttonlist.add(button4);

		button5.setText("Fund Value");
		button5.setPostback("Fundvalue");
		button5.setLink("");
		innerbuttonlist.add(button5);

		button6.setText("Cash Value");
		button6.setPostback("csv");
		button6.setLink("");
		innerbuttonlist.add(button6);

		button7.setText("Premium Payment Term");
		button7.setPostback("ppt");
		button7.setLink("");
		innerbuttonlist.add(button7);

		button8.setText("Renewal Payment Procedure");
		button8.setPostback("renewalpayment");
		button8.setLink("");
		innerbuttonlist.add(button8);

		button9.setText("Premium Statement");
		button9.setPostback("premiumreceipt");
		button9.setLink("");
		innerbuttonlist.add(button9);

		button10.setText("Locate Us");
		button10.setPostback("locateus");
		button10.setLink("");
		innerbuttonlist.add(button10);

		button11.setText("Loan Inquiry");
		button11.setPostback("LoanInquiry");
		button11.setLink("");
		innerbuttonlist.add(button11);

		button12.setText("Update Personal Details");
		button12.setPostback("UpdatePersonalDetails");
		button12.setLink("");
		innerbuttonlist.add(button12);
		
		button13.setText("NAV Registration");
		button13.setPostback("NavRegistration");
		button13.setLink("");
		innerbuttonlist.add(button13);
		
		button14.setText("NAV Deregistration");
		button14.setPostback("NavDeregistration");
		button14.setLink("");
		innerbuttonlist.add(button14);

		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);

		return innerData;
	}
	@Override
	public InnerData getButtonsYesNo(String action) 
	{
		innerbuttonlist=new ArrayList<InnerButton>();
		innerData=new InnerData();
		
		button2.setText("Yes");
		button2.setPostback("yes");
		button2.setLink("");
		innerbuttonlist.add(button2);

		button.setText("No");
		button.setPostback("no");
		button.setLink("");
		innerbuttonlist.add(button);

		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);

		return innerData;
	}
	@Override
	public InnerData getButtonsRenewalPaymentProcedure(String action) 
	{
		innerbuttonlist=new ArrayList<InnerButton>();
		innerData=new InnerData();
		
		button.setText("Pay Online");
		button.setPostback("pay online");
		button.setLink("");
		innerbuttonlist.add(button);

		button2.setText("Direct Debit");
		button2.setPostback("direct debit");
		button2.setLink("");
		innerbuttonlist.add(button2);

		button3.setText("CreditCard Standing Instructions");
		button3.setPostback("credit");
		button3.setLink("");
		innerbuttonlist.add(button3);

		button4.setText("Other Procedure");
		button4.setPostback("other procedure");
		button4.setLink("");
		innerbuttonlist.add(button4);

		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);
		return innerData;
	}
	@Override
	public InnerData getButtonsLoanInquiry(String action) 
	{
		innerbuttonlist=new ArrayList<InnerButton>();
		innerData=new InnerData();
		
		button.setText("Loan Eligibility Details");
		button.setPostback("loanegigibilitydetails");
		button.setLink("");
		innerbuttonlist.add(button);

		button2.setText("Outstanding Loan Details");
		button2.setPostback("outstandingloandetials");
		button2.setLink("");
		innerbuttonlist.add(button2);

		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);
		return innerData;
	}
	@Override
	public InnerData getButtonsUpdatePersonalDetails(String action) 
	{
		innerbuttonlist=new ArrayList<InnerButton>();
		innerData=new InnerData();

		button.setText("Update Mobile");
		button.setPostback("UpdateMobile");
		button.setLink("");
		innerbuttonlist.add(button);

		button2.setText("Update Email");
		button2.setPostback("UpdateEmail");
		button2.setLink("");
		innerbuttonlist.add(button2);
		
		button3.setText("Address Change");
		button3.setPostback("AddressChange");
		button3.setLink("");
		innerbuttonlist.add(button3);
		
		button4.setText("Nominee Change");
		button4.setPostback("NomineeChange");
		button4.setLink("");
		innerbuttonlist.add(button4);

		/*button3.setText("Update Aadhaar");
		button3.setPostback("UpdateAadhaar");
		button3.setLink("");
		innerbuttonlist.add(button3);*/

		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);
		return innerData;
	}

	@Override
	public InnerData getButtonsUpdateAadhaar_MobileEntered_EmailEntered(String action) 
	{
		innerbuttonlist=new ArrayList<InnerButton>();
		innerData=new InnerData();
		
		button2.setText("Yes");
		button2.setPostback("yes");
		button2.setLink("");
		innerbuttonlist.add(button2);

		button.setText("No");
		button.setPostback("no");
		button.setLink("");
		innerbuttonlist.add(button);

		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);
		return innerData;
	}
	@Override
	public InnerData getPayOnline(String action)
	{
		innerbuttonlist=new ArrayList<InnerButton>();
		innerData=new InnerData();
		
		button.setText("Premium Due");
		button.setPostback("premiumdue");
		button.setLink("");
		innerbuttonlist.add(button);

		button2.setText("Policy Term");
		button2.setPostback("maturity and term");
		button2.setLink("");
		innerbuttonlist.add(button2);

		button3.setText("Policy Pack");
		button3.setPostback("policypack");
		button3.setLink("");
		innerbuttonlist.add(button3);

		button4.setText("Maturity");
		button4.setPostback("maturity and term");
		button4.setLink("");
		innerbuttonlist.add(button4);

		button5.setText("Fund Value");
		button5.setPostback("Fundvalue");
		button5.setLink("");
		innerbuttonlist.add(button5);

		button6.setText("Cash Value");
		button6.setPostback("csv");
		button6.setLink("");
		innerbuttonlist.add(button6);

		button7.setText("Policy Payment Term");
		button7.setPostback("ppt");
		button7.setLink("");
		innerbuttonlist.add(button7);

		button8.setText("Renewal Payment Procedure");
		button8.setPostback("renewalpayment");
		button8.setLink("");
		innerbuttonlist.add(button8);

		button9.setText("Premium Receipt");
		button9.setPostback("premiumreceipt");
		button9.setLink("");
		innerbuttonlist.add(button9);

		button10.setText("Locate Us");
		button10.setPostback("locateus");
		button10.setLink("");
		innerbuttonlist.add(button10);

		button11.setText("Loan Inquiry");
		button11.setPostback("LoanInquiry");
		button11.setLink("");
		innerbuttonlist.add(button11);

		button12.setText("Update Personal Details");
		button12.setPostback("UpdatePersonalDetails");
		button12.setLink("");
		innerbuttonlist.add(button12);
		
		button13.setText("NAV Registration");
		button13.setPostback("NavRegistration");
		button13.setLink("");
		innerbuttonlist.add(button13);
		
		button14.setText("NAV Deregistration");
		button14.setPostback("NavDeregistration");
		button14.setLink("");
		innerbuttonlist.add(button14);

		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);
		return innerData;
	}
	@Override
	public InnerData getHelpfulSomeHelpful(String action)
	{
		innerbuttonlist=new ArrayList<InnerButton>();
		innerData=new InnerData();
		
		button10.setText("Helpful");
		button10.setPostback("helpful");
		button10.setLink("");
		innerbuttonlist.add(button10);

		button11.setText("Somewhat helpful");
		button11.setPostback("somewhatHelpful");
		button11.setLink("");
		innerbuttonlist.add(button11);

		button12.setText("Not helpful");
		button12.setPostback("notHelpful");
		button12.setLink("");
		innerbuttonlist.add(button12);

		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);
		return innerData;
	}
	
	// cs button call
	@Override
	public InnerData getButtonFromDB(String result)
	
	{   int i=0;
	    JSONObject buttonObj = new JSONObject(result);
	    Object csbot= buttonObj.get("csbotButtonCallPayloadResponse");
	    JSONArray array = (JSONArray) csbot; 
	    innerbuttonlist=new ArrayList<InnerButton>();
	    innerData=new InnerData();
		
		for (i=0;i<array.length();i++)
		{
		button = new InnerButton();
		button.setText(array.get(i)+"");
		button.setPostback(array.get(i)+"");
		button.setLink("");
		innerbuttonlist.add(button);
		
		}
		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);
		return innerData;
	}
	
	@Override
	public InnerData getButtonBuyNow(String result, String url)
	
	{   int i=0;
	    JSONObject buttonObj = new JSONObject(result);
	    Object csbot= buttonObj.get("csbotButtonCallPayloadResponse");
	    JSONArray array = (JSONArray) csbot; 
	    innerbuttonlist=new ArrayList<InnerButton>();
	    innerData=new InnerData();
		
		for (i=0;i<array.length();i++)
		{
		button = new InnerButton();
		button.setText(array.get(i)+"");
		button.setPostback(array.get(i)+"");
		button.setLink("");
		innerbuttonlist.add(button);
		
		}
		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);
		
		return innerData;
	}
	
	@Override
	public InnerData getButtonOnlineOffline(String result/*, String BuyNowUrl, String KnowMoreUrl*/)
	
	{   int i=0;
		int j=0;
		String type="";
		String description="";
		String dbBuyNowUrl="";
		String dbKnowMoreUrl="";
	    JSONObject buttonObj = new JSONObject(result);
	    Object csbot= buttonObj.get("csbotButtonCallPayloadResponse");
	    JSONArray array = (JSONArray) csbot; 
	    innerbuttonlist=new ArrayList<InnerButton>();
	    innerData=new InnerData();
	    for (i=0;i<array.length();i++)
		{
	    	JSONArray innerArray=(JSONArray) array.get(i);
	    	for(j=0;j<innerArray.length();j++)
	    	{
	    	if(j==0)
	    	{
	    		type=innerArray.get(j)+"";
	    	}
	    	else if(j==2)
	    	{
	    		dbBuyNowUrl=innerArray.get(j)+"";
	    	}
	    	else if(j==3)
	    	{
	    		dbKnowMoreUrl=innerArray.get(j)+"";
	    	}
		}
		}
			
			if("online".equalsIgnoreCase(type))
			{
				button.setText("Buy Now");
				button.setPostback("Buy Now");
				button.setLink(dbBuyNowUrl);
				innerbuttonlist.add(button);
				
				button2.setText("Know More");
				button2.setPostback("Know More Online");
				button2.setLink(dbKnowMoreUrl);
				innerbuttonlist.add(button2);
			}
			
			else if ("offline".equalsIgnoreCase(type))
			{
				button.setText("Request an Agent");
				button.setPostback("Request an Agent");
				button.setLink("https://www.maxlifeinsurance.com/contact-us");
				innerbuttonlist.add(button);
				
				button2.setText("Know More");
				button2.setPostback("Know More Offline");
				button2.setLink(dbKnowMoreUrl);
				innerbuttonlist.add(button2);
			}
	    

		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);
		
		return innerData;
	}
	
	@Override
	public InnerData getButtonHello(String action)
	{
		innerbuttonlist=new ArrayList<InnerButton>();
		innerData=new InnerData();

		button.setText("Service Existing Policy");
		button.setPostback("Service Existing Policy");
		button.setLink("");
		innerbuttonlist.add(button);

		button2.setText("Check out New Products");
		button2.setPostback("Check out New Products");
		button2.setLink("");
		innerbuttonlist.add(button2);

		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);

		return innerData;
	}
	

	
}
