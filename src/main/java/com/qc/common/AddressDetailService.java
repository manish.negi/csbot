package com.qc.common;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class AddressDetailService 
{
	private static Logger logger = LogManager.getLogger(AddressDetailService.class);

	public String AddressDetail(String pincode)
	{
		ResourceBundle res = ResourceBundle.getBundle("errorMessages");
		String applicationurl=res.getString("javalocateusurl");
		String output = new String();
		StringBuilder result = new StringBuilder();
		StringBuilder requestdata = new StringBuilder();
		HttpURLConnection conn = null;
		try{
			XTrustProvider trustProvider = new XTrustProvider();
			trustProvider.install();
			URL url = new URL(applicationurl);
			conn = (HttpURLConnection) url.openConnection();
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			requestdata.append(" 	{	 ");
			requestdata.append(" 	      \"payload\": {	 ");
			requestdata.append(" 	         \"pinCode\": \"").append(pincode).append("\"");
			requestdata.append(" 	   }	 ");
			requestdata.append(" 	}	 ");
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(requestdata.toString());
			writer.flush();
			try {
				writer.close();
			} catch (Exception e1) {
			}

			int apiResponseCode = conn.getResponseCode();
			logger.info("API Response Code : - " + apiResponseCode);
			if (apiResponseCode == 200) 
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
				logger.info("External API Call : END");
			}
		}
		catch(Exception e)
		{
			logger.info("Exception Occoured While Calling API's " + e);
		}
		return result.toString();
	}
	
	// Get button from DB by calling service
	public String buttonService(String action, String lifeStages, String goals, String productName)
	{
		ResourceBundle res = ResourceBundle.getBundle("errorMessages");
		String applicationurl=res.getString("getbuttonurl");
		String output = new String();
		StringBuilder result = new StringBuilder();
		StringBuilder requestdata = new StringBuilder();
		HttpURLConnection conn = null;
		try{
			XTrustProvider trustProvider = new XTrustProvider();
			trustProvider.install();
			//UUID uniqueId = UUID.randomUUID();
			URL url = new URL(applicationurl);
			conn = (HttpURLConnection) url.openConnection();
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			requestdata.append("	{	");
			requestdata.append("	  \"header\": {	");
			requestdata.append("	    \"correlationId\": \""+1234567890+"\",	");
			requestdata.append("	    \"msgVersion\": \"\",	");
			requestdata.append("	    \"appId\": \"\",	");
			requestdata.append("	    \"userId\": \"\",	");
			requestdata.append("	    \"password\": \"\",	");
			requestdata.append("	    \"rollId\":\"\"	");
			requestdata.append("	  },	");
			requestdata.append(" 	      \"payload\": {	 ");
			requestdata.append("	    \"action\": \""+action+"\",");
			requestdata.append("	    \"lifeStages\": \""+lifeStages+"\",");
			requestdata.append("	    \"goals\": \""+goals+"\",");
			requestdata.append("	    \"productName\": \""+productName+"\"");
			requestdata.append(" 	   }	 ");
			requestdata.append(" 	}	 ");
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(requestdata.toString());
			writer.flush();
			try {
				writer.close();
			} catch (Exception e1) {
			}

			int apiResponseCode = conn.getResponseCode();
			logger.info("API Response Code : - " + apiResponseCode);
			if (apiResponseCode == 200) 
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
				logger.info("External API Call : END");
			}
		}
		catch(Exception e)
		{
			logger.info("Exception Occoured While Calling API's " + e);
		}
		return result.toString();
	}
}
