package com.qc.common;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;

@Component
public class DateValidator 
{
	/*public static void main(String...ar)
	{
		boolean result = new DateValidator().isThisDateValid("01-jan-2012");
		System.out.println(result);
	}*/
	public boolean isThisDateValid(String dateToValidate)
	{
		try{
			if(dateToValidate == null)
			{
				return false;
			}
			else
			{
				DateTimeFormatter.ofPattern("dd-MM-yyyy").parse(dateToValidate);
			}
		}catch(Exception ex)
		{
			return false;
		}
		return true;
	}
}