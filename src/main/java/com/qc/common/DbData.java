package com.qc.common;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class DbData 
{
	public String getDbData(String result)
	{
		String value="";
		JSONObject buttonObj = new JSONObject(result);
		Object csbot= buttonObj.get("csbotButtonCallPayloadResponse");
		JSONArray array = (JSONArray) csbot; 
		for (int i=0;i<array.length();i++)
		{
			value=array.get(i).toString();
		}
		return value;
	}

}
