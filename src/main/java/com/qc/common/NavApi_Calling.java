package com.qc.common;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;

import javax.net.ssl.HttpsURLConnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.qc.api.service.PolicyInfo_Handler_Service;

@Component
public class NavApi_Calling {

	public static ResourceBundle res = ResourceBundle.getBundle("errorMessages");
	private static Logger logger = LogManager.getLogger(PolicyInfo_Handler_Service.class);
	
	public String navFundDetail(String policyNo)
	{
		String  DevMode = "N";
		StringBuilder resultData = new StringBuilder();
		String output = new String();
		HttpURLConnection conn = null;
		try
		{
			
			String serviceurl = res.getString("getfund_detail_url");
			System.out.println("SERVICE URL :-"+serviceurl);
			URL url = new URL(serviceurl);
			if(DevMode!=null && !"".equalsIgnoreCase(DevMode) && "Y".equalsIgnoreCase(DevMode))
			{
				Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("cachecluster.maxlifeinsurance.com", 3128));
				conn = (HttpURLConnection) url.openConnection(proxy);
			}else{
				conn = (HttpURLConnection) url.openConnection();
			}
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");

			StringBuilder sb=new StringBuilder();
			sb.append("{ ");
			sb.append("\"request\": { ");
			sb.append("\"header\": { ");
			sb.append("\"soaCorrelationId\": \"225163708\",");
			sb.append("\"soaAppId\": \"NEO\"");
			sb.append("}, ");
			sb.append("\"payload\": { ");
			sb.append("\"policyNo\": \""+policyNo+"\" ");
			sb.append("}");
			sb.append("}");
			sb.append("}");
			System.out.println("START External API Call : navFundDetail");
			logger.info("START External API Call : navFundDetail");
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(sb.toString());
			writer.flush();
			try {writer.close(); } catch (Exception e1) {}
			int apiResponseCode = conn.getResponseCode();
			System.out.println("API Response Code : navFundDetail"+ apiResponseCode);
			if(apiResponseCode == 200)
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				while ((output = br.readLine()) != null) 
				{
					resultData.append(output);
				}
				conn.disconnect();
				br.close();
				System.out.println("END External API Call : navFundDetail");
				logger.info("END External API Call : navFundDetail");
			}
			else
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
				while ((output = br.readLine()) != null) {
					resultData.append(output);
				}
				conn.disconnect();
				br.close();
			}
			if(apiResponseCode == 200)
			{
				Map personaldetailupdate = new  Commons().getGsonData(resultData.toString());
				Map responseData = (Map)((Map)personaldetailupdate.get("response")).get("payload");
				Map msgInfo = (Map)((Map)personaldetailupdate.get("response")).get("msgInfo");
				if("200".equalsIgnoreCase(msgInfo.get("msgCode")+"")
						&& "Success".equalsIgnoreCase(msgInfo.get("msg")+""))
				{
					String result=responseData.get("navRegistrationStatus")+"";
					return result;
				}
				else
				{
					return "Getting error : while calling backend service:: msgCode is !200";
				}
			}
			else
			{
				return "There is some communication glitch! Please try again after some time. Error Code -MAX00NU";
			}
		}catch(Exception ex)
		{
			return "There is some communication glitch! Please try again after some time. Error Code -EXMAX00NU";
		}
	
	}

	public String navUpdateApi(String policyNo,String transactionType)
	{
		String  DevMode = "N";
		StringBuilder resultData = new StringBuilder();
		String output = new String();
		HttpURLConnection conn = null;
		try
		{
			String serviceurl = res.getString("nav_update_url");
			System.out.println("SERVICE URL :-"+serviceurl);
			URL url = new URL(serviceurl);
			if(DevMode!=null && !"".equalsIgnoreCase(DevMode) && "Y".equalsIgnoreCase(DevMode))
			{
				Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("cachecluster.maxlifeinsurance.com", 3128));
				conn = (HttpURLConnection) url.openConnection(proxy);
			}else{
				conn = (HttpURLConnection) url.openConnection();
			}
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");

			StringBuilder sb=new StringBuilder();
			sb.append("{ ");
			sb.append("\"request\": { ");
			sb.append("\"header\": { ");
			sb.append("\"soaCorrelationId\": \"225163708\",");
			sb.append("\"soaAppId\": \"NEO\"");
			sb.append("}, ");
			sb.append("\"payload\": { ");
			sb.append("\"transactionType\": \""+transactionType+"\",");
			sb.append("\"policyNo\": \""+policyNo+"\" ");
			sb.append("}");
			sb.append("}");
			sb.append("}");
			System.out.println("START External API Call : navUpdateApi");
			logger.info("START External API Call : navUpdateApi");
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(sb.toString());
			writer.flush();
			try {writer.close(); } catch (Exception e1) {}
			int apiResponseCode = conn.getResponseCode();
			System.out.println("API Response Code : navUpdateApi"+ apiResponseCode);
			if(apiResponseCode == 200)
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				while ((output = br.readLine()) != null) 
				{
					resultData.append(output);
				}
				conn.disconnect();
				br.close();
				System.out.println("END External API Call : navUpdateApi");
				logger.info("END External API Call : navUpdateApi");
			}
			else
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
				while ((output = br.readLine()) != null) {
					resultData.append(output);
				}
				conn.disconnect();
				br.close();
			}
			if(apiResponseCode == 200)
			{
				Map personaldetailupdate = new  Commons().getGsonData(resultData.toString());
				Map responseData = (Map)((Map)personaldetailupdate.get("response")).get("msgInfo");
				if("200".equalsIgnoreCase(responseData.get("msgCode")+"")
						&& "Success".equalsIgnoreCase(responseData.get("msg")+""))
				{
					if("Activate".equalsIgnoreCase(transactionType))
					{
					return "Thank you! Your policy has been registered for �daily� alerts.</Br>"
							+"Is there anything else I can assist you with?";
					}
					else
					{
						return "Your policy has been deregistered from receiving NAV alerts.</Br>" + 
								"Is there anything else I can assist you with? ";
					}
				}
				else
				{
					return "Sorry, I am unable to process your request. Kindly try again after some time.</Br>" + 
							"Alternatively, you can call our customer care on 1860-120-5577 or write to us at service.helpdesk@maxlifeinsurance.com from your registered email id";
				}
			}
			else
			{
				return "There is some communication glitch! Please try again after some time. Error Code -MAX00UN";
			}
		}catch(Exception ex)
		{
			return "There is some communication glitch! Please try again after some time. Error Code -EXMAX00UN";
		}
	
	}

}
