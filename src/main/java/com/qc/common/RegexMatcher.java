package com.qc.common;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

@Component
public class RegexMatcher 
{
	public int regexpattern(String str)
	{
		Pattern pattern = Pattern.compile(".*[^0-9].*");
		if( !pattern.matcher(str).matches())
		{
			Pattern digitPattern = Pattern.compile("\\d{6}");  
			if(digitPattern.matcher(str).matches())
			{
				return 1;
			}
			else
			{
				return 2;
			}
		}
		else
		{
			return 3;
		}
	}
	public String regexPatternStatus(String Pincode)
	{
		int matcher = regexpattern(Pincode);
		if(matcher==2)
		{
			return "Looks like you have entered incorrect PIN code, please enter valid PIN code.";
		}
		else if(matcher==3)
		{
			return "Looks like you have entered incorrect PIN code, please enter valid PIN code.";
		}
		return "matched";
	}
	
	public boolean isValidEmailAddress(String email){
		   
		 
	        boolean aaaaa = true ;
	    	if(Pattern.matches("\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b",email)){
	    	
	    		
	    		if(true)
	    		{
	    			String [] a= email.split("@");
	    			String d=a[0];
	    			
	    			if(isNumeric(d))
	    			{
	    				aaaaa= false;
	    			}
	    			else
	    			{
	    				aaaaa= true;
	    			}
	    		}
	    		
	    		
	    	}else
	    		aaaaa=false;
	    	
	    	return aaaaa;
	    }
	   
		public static boolean isNumeric(String d)
		{
		  return d.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
		}
	    
}
