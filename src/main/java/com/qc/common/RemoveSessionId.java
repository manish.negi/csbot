package com.qc.common;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qc.controller.ChatBotController;

@Service
public class RemoveSessionId 
{
	@Autowired
	private CalculateTimeDiff calculateTimeDiff;
	public void removedSessionIdfromCash(Map<String, Object> serviceResp, Map<String, Map> responsecache_onsessionId, Map<String, String> mapforcashbeforevalidation,
			Map<String, String> mapforrenewalPayment,Map<String, String> removefromcash, Map<String, String> mobile_email)
	{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		for(Map.Entry<String, String> entry : removefromcash.entrySet())
		{
			String keytoremove = entry.getKey();
			String time = entry.getValue();
			String currentTime = dtf.format(now);
			System.out.println("Current Time is :-"+currentTime);
			long diffminutes = calculateTimeDiff.timediff(currentTime, time);
			if(diffminutes > 30)
			{
				removefromcash.remove(keytoremove);
				responsecache_onsessionId.remove(keytoremove);
				serviceResp.remove(keytoremove);
				mapforcashbeforevalidation.remove(keytoremove);
				mapforrenewalPayment.remove(keytoremove);
			}
		}
	}
}
