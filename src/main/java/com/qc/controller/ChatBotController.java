package com.qc.controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qc.Interceptors.InputCTP;
import com.qc.Interceptors.InputFund;
import com.qc.Interceptors.InputMaturity;
import com.qc.Interceptors.InputPolicyPack;
import com.qc.Interceptors.InputReceipt;
import com.qc.Interceptors.InputSurrender;
import com.qc.Interceptors.LifeStage;
import com.qc.Interceptors.LocateusPinCode;
//import com.qc.InterceptorsImpl.DbData;
import com.qc.Interceptors.PremiumPaymentTerm;
import com.qc.api.hello.InnerData;
import com.qc.api.response.WebhookResponse;
import com.qc.api.service.AddressDetail;
import com.qc.api.service.MaturityDate;
import com.qc.api.service.NavDeregistration;
import com.qc.api.service.NavRegistration;
import com.qc.api.service.OTP_Handler_Service_UAT;
import com.qc.api.service.PersonalDetailUpdate;
import com.qc.api.service.PolicyInfo_Handler_Service;
import com.qc.api.service.PolicyLoanDetail;
import com.qc.buttonImpl.ButtonImpl;
import com.qc.common.AddressDetailService;
import com.qc.common.Adoptionlogs;
import com.qc.common.DateValidator;
import com.qc.common.DbData;
import com.qc.common.RegexMatcher;
import com.qc.common.RemoveSessionId;

@Controller
@RequestMapping("/webhook")
public class ChatBotController {

	private static Logger logger = LogManager.getLogger(ChatBotController.class);
	public static ResourceBundle resProp = ResourceBundle.getBundle("errorMessages");
	public Map<String, Object> serviceResp = new HashMap<String, Object>();
	public Map<String, Map> responsecache_onsessionId = new ConcurrentHashMap<String, Map>();
	public Map<String, String> mapforcashbeforevalidation = new ConcurrentHashMap<String, String>();
	public Map<String, String> mapforrenewalPayment = new ConcurrentHashMap<String, String>();
	public Map<String, String> removefromcash = new ConcurrentHashMap<String, String>();
	public Map<String, String> mobile_email = new ConcurrentHashMap<String, String>();
	public Map<String, String> goalButtons = new ConcurrentHashMap<String, String>();
	public Map<String, String> productButtons = new ConcurrentHashMap<String, String>();

	/*----------------------------------------------------*/
	// For Production
	/*
	 * @Autowired OTP_Handler_Service oTP_Handler_Service;
	 */
	/*----------------------------------------------------*/
	// For UAT
	@Autowired
	OTP_Handler_Service_UAT oTP_Handler_Service;
	/*----------------------------------------------------*/
	/*@Autowired
	PolicyDetail_Handler_Service policyDetail_Handler_Service;*/
	@Autowired
	PolicyInfo_Handler_Service policyInfo_Handler_Service;
	/*@Autowired
	MliDoc_Handler_Service mliDoc_Handler_Service;*/
	@Autowired
	MaturityDate maturityDate;
	@Autowired
	AddressDetail addressDetail;
	@Autowired
	AddressDetailService addressDetailService;
	@Autowired
	private Adoptionlogs adoption;
	@Autowired
	private RemoveSessionId removeSessionId;
	@Autowired
	private PolicyLoanDetail policyLoan;
	@Autowired
	private DateValidator dateValidator;
	@Autowired
	PersonalDetailUpdate personalDetailUpdate;
	@Autowired
	RegexMatcher regexMatcher;
	@Autowired
	private ButtonImpl buttons;
	@Autowired
	private DbData dbData;
	@Autowired
	private PremiumPaymentTerm premiumPaymentTerm;
	@Autowired
	private InputCTP inputCTP;
	@Autowired
	private InputSurrender inputSurrender;
	@Autowired
	private InputReceipt inputReceipt;
	@Autowired
	private InputFund inputFund;
	@Autowired
	private InputMaturity inputMaturity;
	@Autowired
	private InputPolicyPack inputPolicyPack;
	@Autowired
	private LocateusPinCode locateusPinCode;
	@Autowired
	private LifeStage lifeStage;
	
	@Autowired
	private NavRegistration navRegistration;
	
	@Autowired
	private NavDeregistration navDeregistration;
	@Scheduled(cron = "0 0/30 * * * ?")
	public void schedular() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		logger.info("Spring Schedular START ::" + dtf.format(now));
		removeSessionId.removedSessionIdfromCash(serviceResp, responsecache_onsessionId, mapforcashbeforevalidation,
				mapforrenewalPayment, removefromcash, mobile_email);
		logger.info("Spring Schedular END ::" + dtf.format(now));
	}

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody WebhookResponse webhook(@RequestBody String obj) {

		String speech = null;
		logger.info("---Inside the Controller----");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		InnerData innerData = new InnerData();
		String regex = "^[0-9]{10}$";
		String sessionId = "";
		String action = "";
		String policy_Number = "";
		String resolvedQuery = "";
		String pincode = "";
		String dob = "";
		String newMobileNumber = "";
		String newEmail = "";
		String LifeStages = "";
		String Goals = "";
		String ProductName = "";
		try {
			logger.info("Controller : ChatBotController : START");
			JSONObject object = new JSONObject(obj.toString());
			sessionId = object.get("sessionId") + "";
			logger.info("Request Session Id :- " + sessionId);
			action = object.getJSONObject("result").get("action") + "";
			logger.info("--------------Testing----" + action);
			try {
				resolvedQuery = object.getJSONObject("result").get("resolvedQuery") + "";
				logger.info("----Resolved Query----" + resolvedQuery);
			} catch (Exception ex) {
				resolvedQuery = "";
			}

			String lifespan = "";
			String name = "";
			String polcyNumberOrginal = "";
			String Given_PolicyNumber = "";
			ObjectMapper mapperObj = new ObjectMapper();
			try {
				policy_Number = object.getJSONObject("result").getJSONObject("parameters").getJSONObject("PolicyNumber")
						.get("Given-PolicyNumber") + "";
			} catch (Exception e) {
				policy_Number = "";
			}
			try {
				pincode = object.getJSONObject("result").getJSONObject("parameters").get("pin") + "";
				logger.info("pincode --- testing " + pincode);
			} catch (Exception e) {
				pincode = "";
			}
			try {
				dob = object.getJSONObject("result").getJSONObject("parameters").get("dob") + "";
				logger.info("pincode --- testing " + dob);
			} catch (Exception e) {
				dob = "";
			}
			try {
				newMobileNumber = object.getJSONObject("result").getJSONObject("parameters").get("mobile") + "";
				mobile_email.put("newMobileNumber", newMobileNumber);
				logger.info("newMobileNumber --- testing " + newMobileNumber);
			} catch (Exception e) {
				newMobileNumber = "";
			}
			try {
				newEmail = object.getJSONObject("result").getJSONObject("parameters").get("email") + "";
				mobile_email.put("newEmail", newEmail);
				logger.info("newEmail --- testing " + newEmail);
			} catch (Exception e) {
				newEmail = "";
			}
			/*try {
				LifeStages = object.getJSONObject("result").get("resolvedQuery") + "";
				logger.info("LifeStages --- testing " + LifeStages);
			} catch (Exception e) {
				LifeStages = "";
			}
			try {
				Goals = object.getJSONObject("result").get("resolvedQuery") + "";
				logger.info("Goals --- testing " + Goals);
			} catch (Exception e) {
				Goals = "";
			}
			try {
				ProductName = object.getJSONObject("result").get("resolvedQuery") + "";
				logger.info("ProductName --- testing " + ProductName);
			} catch (Exception e) {
				ProductName = "";
			}*/
			logger.info("Request PolicyNo :- " + policy_Number);
			if (true) {
				removefromcash.put(sessionId, dtf.format(now));
				if (!"PolicyNumberValidation".equalsIgnoreCase(action) && !"OTPValidation".equalsIgnoreCase(action)
						&& !"OTP.NotAvailable".equalsIgnoreCase(action) && !"HI".equalsIgnoreCase(action)
						&& !"close.conversation".equalsIgnoreCase(action) && !"dobvalidate".equalsIgnoreCase(action)) {
					mapforcashbeforevalidation.put(sessionId, action);
				}
			}
			switch (action) {
			case "HI":
			case "locateus-pincode.locateus-pincode-yes":
			case "PolicyCTP.PolicyCTP-yes":
			case "PolicySurrenderValue.PolicySurrenderValue-yes":
			case "PolicyPremiumReceipt.PolicyPremiumReceipt-yes":
			case "PolicyFundValue.PolicyFundValue-yes":
			case "InputMaturity.InputMaturity-yes":
			case "InputPPT.InputPPT-yes":
			case "Policypolicydocument.Policypolicydocument-yes":
			case "PolicyPolicyNumberValidation.PolicyPolicyNumberValidation-yes":
			case "Client-DOB.Client-DOB-yes":
			case "MobileEntered.MobileEntered-yes.MobileEntered-yes-yes":
			case "EmailEntered.EmailEntered-yes.EmailEntered-yes-yes":
			case "UpdateAadhar.UpdateAadhar-yes":
			case "credit.credit-yes-2": 
			case "BuyNow.BuyNow-yes":
			case "RequestanAgent.RequestanAgent-yes":
			case "KnowMoreOnline.KnowMoreOnline-yes":
			case "KnowMoreOffline.KnowMoreOffline-yes":
			case "NomineeChange.NomineeChange-yes":
			case "AddressChange.AddressChange-yes":
			case "NAVRegistration.NAVRegistration-yes.NAVRegistration-yes-yes":
			case "NAVDeregistration.NAVDeregistration-yes.NAVDeregistration-yes-yes":
			case "MobileEntered.MobileEntered-no.MobileEntered-no-yes":
			case "EmailEntered.EmailEntered-no.EmailEntered-no-yes":
			{
				logger.info("Action Invoded:- HI");
				if ("HI".equalsIgnoreCase(action)) {
					speech = "Hello I am Mili, your Max Life Assistant. Please select from the options below.";
					innerData = buttons.getButtonHello(action);
				} else {
					speech = "I can help you with following on your Policy";
				}
				// Buttons
				innerData = buttons.getButtonHello(action);
			}
			break;
			case "OTP.NotAvailable": {
				logger.info("CASE :: OTP.NotAvailable :: START");
				if (responsecache_onsessionId.containsKey(sessionId)) {
					logger.info("Action Invoded:- OTP.NotAvailable");
					String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
					Map<String, String> orignalData = oTP_Handler_Service.getPolicyOtp(cachePolicyNo, sessionId, 1);
					if (orignalData.get("policyotp") != null) {
						responsecache_onsessionId.put(sessionId, orignalData);
						speech = orignalData.get("Message");
					}
				} else {
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("CASE :: OTP.NotAvailable :: END");
			}
			break;
			case "PolicyNumberValidation": {
				logger.info("CASE :: PolicyNumberValidation :: START");
				if (responsecache_onsessionId.containsKey(sessionId)) {
					logger.info("Action Invoded:- PolicyNumberValidation");
					String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
					String cachevalidOTP = responsecache_onsessionId.get(sessionId).get("ValidOTP") + "";
					String cacheOTP = responsecache_onsessionId.get(sessionId).get("policyotp") + "";
					String proposerName = responsecache_onsessionId.get(sessionId).get("proposerName") + "";

					logger.info("Inside :: PolicyNumberInSession Check !=null");
					if (cachePolicyNo != null && (cachevalidOTP != null && !"".equalsIgnoreCase(cachevalidOTP))) {
						if (!(policy_Number.equals(cachePolicyNo))) {
							logger.info("A new policy number entered by the customer " + policy_Number
									+ "First clear the Cach " + "then Go to api Call");
							responsecache_onsessionId.clear();
							Map<String, String> orignalData = oTP_Handler_Service.getPolicyOtp(policy_Number, sessionId,
									0);
							responsecache_onsessionId.put(sessionId, orignalData);
							logger.info("END :: Request For Re-Generate PolicyOTP :- " + serviceResp);
							speech = orignalData.get("Message");
							WebhookResponse responseObj = new WebhookResponse(speech, speech, innerData);
							return responseObj;
						}
					} else if (!"".equalsIgnoreCase(cachePolicyNo) && cachePolicyNo != null && cachevalidOTP != null
							&& !cachevalidOTP.equalsIgnoreCase("")) {
						logger.info("Inside 2 Check :: PolicyNo & OTP Validation From ehcache");
						speech = "OTP Verification is completed for Policy Number " + cachePolicyNo
								+ ", please tell what you want to know about policy";
						logger.info("Message Sent to Api :-" + speech);
					} else if (cachePolicyNo != null) {
						String otp_session = "";
						if (!"".equalsIgnoreCase(cacheOTP) && cacheOTP != null) {
							otp_session = cacheOTP;
						}
						if (!"".equalsIgnoreCase(otp_session) && otp_session != null) {
							if (otp_session.equals(policy_Number)) {
								String policyBasePlanIdDesc = "";
								String lastPremPmtDt = "";
								String actionWithOutHi = "";

								Map data = policyInfo_Handler_Service.getPolicyInfo(cachePolicyNo);
								try {
									String Json = mapperObj.writeValueAsString(data);
									JSONObject jsonObject = new JSONObject(Json.toString());
									policyBasePlanIdDesc = jsonObject.getJSONObject("PolicyData")
											.get("policyBasePlanIdDesc") + "";
									lastPremPmtDt = jsonObject.getJSONObject("PolicyData").get("lastPremPmtDt") + "";
								} catch (IOException e) {
									logger.info(e);
								}
								Map<String, Object> serviceResp = responsecache_onsessionId.get(sessionId);
								Map<String, String> maturityData = maturityDate.getMaturityDate(cachePolicyNo);
								serviceResp.put("OverAllMaturityCashData", maturityData);
								serviceResp.put("ValidOTP", cachePolicyNo);
								serviceResp.put("lastPremPmtDt", lastPremPmtDt);
								serviceResp.put("PolData", data);
								serviceResp.remove("policyotp");
								responsecache_onsessionId.put(sessionId, serviceResp);
								for (Map.Entry<String, String> entry : mapforcashbeforevalidation.entrySet()) {
									String key = entry.getKey();
									if (key.equalsIgnoreCase(sessionId)) {
										action = entry.getValue();
										actionWithOutHi = entry.getValue();
									}

								}
								/*speech = "Hi " + Commons.toCamelCase(proposerName) + resProp.getString("welcomeUser")
										+ policyBasePlanIdDesc + " " + resProp.getString("welcomeUser1");*/
								speech="Hello I am Mili, your Max Life Assistant. Please select from the options below.";
								if ("".equalsIgnoreCase(actionWithOutHi) || actionWithOutHi.isEmpty()) {
									// Buttons
									innerData = buttons.getButtonHello("AllButton");
								}

							} else {
								speech = resProp.getString("OTPnotmatch");
							}
						} else {
							speech = resProp.getString("GenerateOTP");
						}
					}
				} else {
					logger.info("User Enter First Time to Generate OTP");
					logger.info("START :: Request For Generate PolicyOTP");
					Map<String, String> orignalData = oTP_Handler_Service.getPolicyOtp(policy_Number, sessionId, 0);
					if (!orignalData.get("Message").contains("Oops")) {
						responsecache_onsessionId.put(sessionId, orignalData);
						logger.info("END :: Request For Generate PolicyOTP :- " + serviceResp);
						speech = orignalData.get("Message");
					} else {
						speech = orignalData.get("Message");
					}
				}
				logger.info("CASE :: PolicyNumberValidation :: END");
			}
			break;
			case "OTPValidation": {
				logger.info("CASE :: OTPValidation :: START");
				if (responsecache_onsessionId.containsKey(sessionId)) {
					logger.info("Action Invoded:- OTPValidation");
					String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
					String cachevalidOTP = responsecache_onsessionId.get(sessionId).get("ValidOTP") + "";
					String cacheOTP = responsecache_onsessionId.get(sessionId).get("policyotp") + "";
					if ("".equalsIgnoreCase(cachePolicyNo) || cachePolicyNo == null) {
						speech = resProp.getString("validPolicyMessage");
					} else if (!"".equalsIgnoreCase(cachevalidOTP) && cachevalidOTP != null) {
						speech = "OTP Verification is completed for Policy Number " + cachePolicyNo
								+ ", please tell what you want to know about policy  OR write reset to start new session";
					} else {
						String otp_session = "";
						String OTP_request = object.getJSONObject("result").getJSONObject("parameters")
								.getJSONObject("OTP").get("Provided-OTP") + "";
						otp_session = cacheOTP;
						if (!"".equalsIgnoreCase(otp_session) && otp_session != null) {
							if (otp_session.equals(OTP_request)) {
								speech = "Mr. Arun. What information you want to know about your policy";
								Map data = policyInfo_Handler_Service.getPolicyInfo(cachePolicyNo);

								Map<String, String> serviceResp = responsecache_onsessionId.get(sessionId);
								serviceResp.put("cachevalidOTP", OTP_request);
								serviceResp.remove("policyotp");
								responsecache_onsessionId.put(sessionId, serviceResp);
							} else {
								speech = "OTP did not match.Please provide correct OTP.";
							}
						} else {
							speech = "You have not generated OTP.Please provide valid policy to generate OTP";
						}
					}
				} else {
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("CASE :: OTPValidation :: END");
			}
			break;
			case "ServiceExistingPolicy": 
			{
				logger.info("Action :: ServiceExistingPolicy start");
				speech = "I'm happy to help you with the following options.";
				innerData = buttons.getButtonsHI(action);
				logger.info("Action :: ServiceExistingPolicy end");
			}
			break;

			case "CheckoutNewProducts": 
			{
				action = "LIFE STAGE";
				logger.info("Action :: CheckoutNewProducts start");
				speech = "To help me assist you better, please choose your current life stage:";
				String result=addressDetailService.buttonService(action, LifeStages, Goals, ProductName);
				innerData = buttons.getButtonFromDB(result);
				logger.info("Action :: CheckoutNewProducts end");
			}
			break;

			case "LifeStage": 
			{
				action = "GOALS";
				goalButtons.put("LifeStages", resolvedQuery);
				LifeStages=goalButtons.get("LifeStages");
				logger.info("Action :: LifeStage start");
				speech=lifeStage.getLifeStage(LifeStages);
				String result=addressDetailService.buttonService(action, LifeStages, Goals, ProductName);
				innerData = buttons.getButtonFromDB(result);
				logger.info("Action :: LifeStage end");
			}
			break;

			case "Goals": 
			{
				logger.info("Action Invoded :: Goals start");
				action = "PRODUCT NAME";
				goalButtons.put("Goals", resolvedQuery);
				LifeStages=goalButtons.get("LifeStages");
				Goals=goalButtons.get("Goals");
				logger.info("Inside Action :: Goals start");
				speech = "Here�s what I have to meet your specific goal:";
				String result=addressDetailService.buttonService(action, LifeStages, Goals, ProductName);
				innerData = buttons.getButtonFromDB(result);
				logger.info("Action :: Goals end");
			}
			break;

			case "ProductName": 
			{
				logger.info("Action Invoded :: ProductName start");
				action = "ONLINE OFFLINE";
				goalButtons.put("Productname", resolvedQuery);
				LifeStages=goalButtons.get("LifeStages");
				Goals=goalButtons.get("Goals");
				ProductName=goalButtons.get("Productname");
				logger.info("Action :: ProductName start");
				speech = "It's a great choice! ";
				String result=addressDetailService.buttonService(action, LifeStages, Goals, ProductName);
				action="DESCRIPTION";
				String description=addressDetailService.buttonService(action, LifeStages, Goals, ProductName);
				String DbData=dbData.getDbData(description);
				speech=speech+"\n "+DbData;
				innerData = buttons.getButtonOnlineOffline(result);
				logger.info("Action Invoded:: ProductName end");
			}
			break;

			case "BuyNow": 
			{
				logger.info("Action Invoded :: Buy Now start");
				speech = "Thank you for choosing Max Life Insurance. Is there anything else I can assist you with?";
				innerData = buttons.getButtonsYesNo(action);
				logger.info("Action Invoded :: Buy Now end");
			}
			break;

			case "KnowMoreOnline": 
			{
				logger.info("Action Invoded :: KnowMoreOnline start");
				action = "KNOW MORE ONLINE";
				Goals=goalButtons.get("Goals").toString();
				LifeStages=goalButtons.get("LifeStages").toString();
				ProductName=goalButtons.get("Productname").toString();
				logger.info(" Going to call service to fetch data rfom DB service");
				String result=addressDetailService.buttonService(action, LifeStages, Goals, ProductName);
				String DbData=dbData.getDbData(result);
				speech = "Hope the information helped. <a href='"+DbData+"' target='_blank'> Click here </a> to buy now or is there anything else I can assist you with? ";
				innerData = buttons.getButtonsYesNo(result);
				logger.info("Action Invoded :: KnowMoreOnline end");
			}
			break;

			case "KnowMoreOffline": 
			{
				logger.info("Action :: Know More Offline start");
				speech = "Hope the information helped. <a href='https://www.maxlifeinsurance.com/contact-us' target='_blank'> Click here </a> to request an agent or is there anything else I can assist you with? ";
				innerData = buttons.getButtonsYesNo(action);
				logger.info("Action :: Know More Offline end");
			}
			break;

			case "RequestanAgent": 
			{
				logger.info("Action :: RequestanAgent start");
				speech = "Thank you for choosing Max Life Insurance. Is there anything else I can assist you with?";
				innerData = buttons.getButtonsYesNo(action);
				logger.info("Action :: RequestanAgent end");
			}
			break;

			default:
			}
			switch (action) {
			case "input.CTP": // Premium Due
			{
				logger.info("CASE :: input.CTP :: START");
				if (responsecache_onsessionId.containsKey(sessionId)){
					speech=inputCTP.getInputCTP(sessionId, responsecache_onsessionId);
					if (!speech.contains(
							"You need to provide valid OTP number which has been sent to your registered mobile number for policy")) {
						speech = speech + "<Br/> Is there anything else I can assist you with?";
						innerData=buttons.getButtonsYesNo(action);
					}
				} else {
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("CASE :: input.CTP :: END");
				}
			break;
			case "input.Surrender": 
			{
				logger.info("CASE :: input.Surrender :: START");
			if (responsecache_onsessionId.containsKey(sessionId)) {
				speech=inputSurrender.getInputSurrender(sessionId, responsecache_onsessionId);
				if (!speech.contains(
						"You need to provide valid OTP number which has been sent to your registered mobile number for policy")) {
					speech = speech + "\n Is there anything else I can assist you with?";
					//Buttons
					innerData=buttons.getButtonsYesNo(action);
				} 
			} else {
				speech = resProp.getString("validPolicyMessage");
			}
			logger.info("CASE :: input.Surrender :: END");
			}
			break;
			case "input.Receipt": {

				logger.info("CASE :: input.Receipt :: START");
				if (responsecache_onsessionId.containsKey(sessionId)) {
					speech=inputReceipt.getInputReceipt(sessionId, responsecache_onsessionId);
					if (!speech.contains(
							"You need to provide valid OTP number which has been sent to your registered mobile number for policy")) {
						speech = speech + "\n Is there anything else I can assist you with?";
						innerData=buttons.getButtonsYesNo(action);
					}
				} else {
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("CASE :: input.Receipt :: END");
			}
			break;
			case "input.Fund": {
				logger.info("CASE :: input.Fund :: START");
				if (responsecache_onsessionId.containsKey(sessionId)) {
					speech=inputFund.getInputFund(sessionId, responsecache_onsessionId);
					if(!speech.contains("You need to provide valid OTP number which has been sent to your registered mobile number for policy"))
					{
						speech = speech + "\n Is there anything else I can assist you with?";
						innerData=buttons.getButtonsYesNo(action);
					}
				} else {
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("CASE :: input.Fund :: END");
			}
			break;
			case "Input.Maturity": {

				logger.info("CASE :: Input.Maturity :: START");
				if (responsecache_onsessionId.containsKey(sessionId)) {
					speech=inputMaturity.getInputMaturity(sessionId, responsecache_onsessionId);
					if(!speech.contains("You need to provide valid OTP number which has been sent to your registered mobile number for policy"))
					{
						speech = speech + "\n Is there anything else I can assist you with?";
						innerData=buttons.getButtonsYesNo(action);
					}
				} else {
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("CASE :: Input.Maturity :: END");
			}
			break;
			case "Policy.PremiumPaymentTerm": {
				logger.info("CASE :: Policy.PremiumPaymentTerm :: START");
				if (responsecache_onsessionId.containsKey(sessionId))
				{
				speech = premiumPaymentTerm.getPremiumPaymentTerm(sessionId, responsecache_onsessionId);
				if (!speech.contains(
						"You need to provide valid OTP number which has been sent to your registered mobile number for policy")) {
					speech = speech + "\n Is there anything else I can assist you with?";
					innerData = buttons.getButtonsYesNo(action);
				} 
				}
				else {
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("CASE :: Policy.PremiumPaymentTerm :: END");
			}
			break;
			// For Mail Purpose
			case "Input.PolicyPack": {
				logger.info("CASE :: Input.PolicyPack :: START");
				if (responsecache_onsessionId.containsKey(sessionId)) {
					speech=inputPolicyPack.getInputPolicyPock(sessionId, responsecache_onsessionId);
					if(!speech.contains("You need to provide valid OTP number which has been sent to your registered mobile number for policy")){
						innerData=buttons.getButtonsYesNo(action);
					}
				}else{
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("CASE :: Input.PolicyPack :: END");
			}
			break;
			case "close.clear": {
				removeSessionId.removedSessionIdfromCash(serviceResp, responsecache_onsessionId,
						mapforcashbeforevalidation, mapforrenewalPayment, removefromcash, mobile_email);
			}
			break;
			case "close.conversation": {
				logger.info("Close.Conversation :: START");
				if (responsecache_onsessionId.containsKey(sessionId)) {
					logger.info("Inside Action :- close.conversation");
					for (Map.Entry<String, String> entry : mapforcashbeforevalidation.entrySet()) {
						String key = entry.getKey();
						if (key.equalsIgnoreCase(sessionId)) {
							mapforcashbeforevalidation.remove(sessionId);
						}
					}
					responsecache_onsessionId.remove(sessionId);
					logger.info("Session_Id Removed :-" + sessionId);
					speech = "Thank you for contacting Max Life. Have a great day!";
				} else {
					speech = "Thank you for contacting Max Life. Have a great day!";
				}
				logger.info("Close.Conversation :: END");
			}
			break;
			case "LocateUs": {
				if (true) {
					logger.info("Action Invoded:- Locate Us");
					speech = "Please enter 6 digit pin code to help you with the nearest Max Life office.";
				}
			}
			break;
			case "locateuspincode": {
				logger.info("CASE :: locateuspincode :: START");
				try {
					String status=locateusPinCode.getLocateUsPinCode(pincode);
					if ("matched".equalsIgnoreCase(status)) {
						speech = addressDetail.getAddress(pincode);
						if ("".equalsIgnoreCase(speech) || speech.isEmpty()) {
							speech = " Looks like we don�t have any Max Life office situated near to the PIN code entered by you, "
									+ "<a href='https://bit.ly/2Gc5bLa' target='_blank'> Click Here </a>  to locate nearest office in your city."
									+ "\n Is there anything else I can assist you with?";
							innerData = buttons.getButtonsYesNo(action);
						} else {
							speech = speech.concat("\n Is there anything else I can assis you with?");
							innerData = buttons.getButtonsYesNo(action);
						}
					} else {
						speech = "Looks like you have entered incorrect PIN code, please enter valid PIN code.";
					}
				} catch (Exception ex) {
					logger.info("Exception occoured in Locateuspincode statement :: " + ex);
				}
				logger.info("CASE :: locateuspincode :: END");
			}
			break;
			case "RenewalPaymentProcedure": {
				logger.info("CASE :: locateuspincode :: START");
				speech = "Please select one of the following options: ";
				innerData = buttons.getButtonsRenewalPaymentProcedure(action);
				mapforrenewalPayment.put(sessionId, sessionId);
				logger.info("CASE :: locateuspincode :: END");
			}
			break;
			case "LoanInquiry": {
				logger.info("Action Invoded:- LoanInquiry :: START");
				speech = "Please select one of the following options: ";
				innerData = buttons.getButtonsLoanInquiry(action);
				logger.info("Action Invoded:- LoanInquiry :: END");
			}
			break;
			case "loanegigibilitydetails": {
				logger.info("CASE :: loanegigibilitydetails :: START");
				if (responsecache_onsessionId.containsKey(sessionId)) {
					speech = " Please enter policy holder's date of birth in DD-MM-YYYY format.\n"
							+ " Ex: For 3rd December 1987, please enter 03-12-1987.";
				} else {
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("CASE :: loanegigibilitydetails :: END");
			}
			break;
			case "outstandingloandetials": {
				logger.info("CASE :: outstandingloandetials :: START");
				if (responsecache_onsessionId.containsKey(sessionId)) {
					speech = " Please enter policy holder's date of birth in DD-MM-YYYY format.\n"
							+ " Ex: For 3rd December 1987, please enter 03-12-1987.";
				} else {
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("CASE :: outstandingloandetials :: END");
			}
			break;
			case "dobvalidate": {
				logger.info("CASE :: dobvalidate :: START");
				if (responsecache_onsessionId.containsKey(sessionId)
						&& mapforcashbeforevalidation.containsKey(sessionId)) {
					boolean status = dateValidator.isThisDateValid(dob);
					if (status) {
						String intent = "";
						for (Map.Entry<String, String> entry : mapforcashbeforevalidation.entrySet()) {
							String key = entry.getKey();
							if (key.equalsIgnoreCase(sessionId)) {
								intent = entry.getValue();
							}
						}
						String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
						if ("loanegigibilitydetails".equalsIgnoreCase(intent)) {
							String flag1 = "loanegigibilitydetails";
							speech = policyLoan.policyLoanDetail(cachePolicyNo, dob, flag1);
						} else {
							String flag2 = "outstandingloandetials";
							speech = policyLoan.policyLoanDetail(cachePolicyNo, dob, flag2);
						}
					} else {
						speech = " Please enter policy holder's date of birth in DD-MM-YYYY format.\n"
								+ " Ex: For 3rd December 1987, please enter 03-12-1987.";
					}
				} else {
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("CASE :: dobvalidate :: END");
			}
			break;
			case "UpdatePersonalDetails": {
				logger.info("Action Invoded:- UpdatePersonalDetails :: START");
				speech = "Please select one of the following options: ";
				innerData = buttons.getButtonsUpdatePersonalDetails(action);
				logger.info("Action Invoded:- UpdatePersonalDetails :: END");
			}
			break;
			case "UpdateMobile": {
				logger.info("Action Invoded:- UpdateMobile :: START");
				if (responsecache_onsessionId.containsKey(sessionId)) {
					logger.info("Inside Action :- UpdateMobile");
					speech = "Kindly enter the new ten-digit mobile number to be registered with your Max Life policy. Please do not prefix the number with 91, 0, +91.";
				} else {
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("Action Invoded:- UpdateMobile :: END");
			}
			break;
			case "UpdateEmail": {
				logger.info("Action Invoded:- UpdateEmail :: START");
				if (responsecache_onsessionId.containsKey(sessionId)) {
					logger.info("Inside Action :- UpdateEmail");
					speech = "Kindly enter the email id to be registered with your Max Life policy.";
				} else {
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("Action Invoded:- UpdateEmail :: END");
			}
			break;
			case "UpdateAadhaar": {
				logger.info("Action Invoded:- UpdateAadhaar :: START");
				if (responsecache_onsessionId.containsKey(sessionId)) {
					logger.info("Inside Action :- UpdateAadhaar");
					speech = " <a href='https://goo.gl/KBHnJN' target='_blank'> click here </a> "
							+ " to link your Aadhaar Number with your Max Life policy.\n Is there anything else I can assist you with?";
					innerData = buttons.getButtonsYesNo(action);
				} else {
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("Action Invoded:- UpdateAadhaar :: END");
			}
			break;
			case "AddressChange": {
				logger.info("Action Invoded:- AddressChange :: START");
				if (responsecache_onsessionId.containsKey(sessionId)) {
					logger.info("Inside Action :- AddressChange");
					speech = "Updating address is quick and simple.Please <a href='http://bit.ly/2MG4AAu' target='_blank'> click here </a> "
							+ " to initiate the process.\n Is there anything else I can assist you with?";
					innerData = buttons.getButtonsYesNo(action);
				} else {
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("Action Invoded:- AddressChange :: END");
			}
			break;
			case "NomineeChange": {
				logger.info("Action Invoded:- NomineeChange :: START");
				if (responsecache_onsessionId.containsKey(sessionId)) {
					logger.info(" Inside Action :- NomineeChange");
					speech = "Updating nominee is quick and simple.Please <a href='https://www.maxlifeinsurance.com/customer-service/ServiceRequest/changeNomineeDetails.html' target='_blank'> click here </a> "
							+ " to initiate the process.\n Is there anything else I can assist you with?";
					innerData = buttons.getButtonsYesNo(action);
				} else {
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("Action Invoded:- NomineeChange :: END");
			}
			break;
			case "MobileEntered": {
				logger.info("Action Invoded:- MobileEntered :: START");
				try {
					if (responsecache_onsessionId.containsKey(sessionId)) {
						String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
						//logger.info("newMobileNumber --- testing " + newMobileNumber);
						if (newMobileNumber.matches(regex)) {
							String intent = "MobileNo";

							speech = personalDetailUpdate.getCustomerInfo(cachePolicyNo, intent, newMobileNumber);

							if (speech.contains("There is some communication glitch!")) {
								speech = "There is some communication glitch! Please try again after some time. Error Code -MAX00PLD";
							} else {
								innerData = buttons.getButtonsYesNo(action);
							}
						} else {
							speech = "Sorry! Looks like you've entered an incorrect mobile number. Please enter your mobile number again.";
						}
					} else {
						speech = resProp.getString("validPolicyMessage");
					}
				} catch (Exception e) {
					newMobileNumber = "";
				}
				logger.info("Action Invoded:- MobileEntered :: END");
			}
			break;
			case "MobileEntered.MobileEntered-yes": {
				logger.info("Action Invoded:- MobileEntered.MobileEntered-yes :: START");
				if (responsecache_onsessionId.containsKey(sessionId)) {
					logger.info("Inside Action :- MobileEntered.MobileEntered-yes");
					String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
					String cacheMobile = mobile_email.get("newMobileNumber") + "";
					speech = personalDetailUpdate.updateMobile(cachePolicyNo, cacheMobile);
					innerData = buttons.getButtonsYesNo(action);
				} else {
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("Action Invoded:- MobileEntered.MobileEntered-yes :: END");
			}
			break;
			case "MobileEntered.MobileEntered-no": {
				logger.info("Action Invoded:- MobileEntered.MobileEntered-no :: START");
				speech = "No problem! \n" + 
						"Is there anything else I can assist you with?";
				innerData = buttons.getButtonsYesNo(action);
				logger.info("Action Invoded:- MobileEntered.MobileEntered-no :: END");
			}
			break;
			case "EmailEntered": {
				logger.info("Action Invoded:- EmailEntered :: START");
				try {
					if (responsecache_onsessionId.containsKey(sessionId)) {
						logger.info("Inside Action :- EmailEntered");
						String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
						boolean result = regexMatcher.isValidEmailAddress(newEmail);
						if (result) {
							String intent = "EmailId";
							speech = personalDetailUpdate.getCustomerInfo(cachePolicyNo, intent, newEmail);
							if (speech.contains("There is some communication glitch!")) {
								speech = "There is some communication glitch! Please try again after some time. Error Code -MAX00PLD";
							} else {
								innerData = buttons.getButtonsYesNo(action);
							}
						} else {
							speech = " Sorry! Looks like you've entered an incorrect email id. Please enter your email id again.";
						}
					} else {
						speech = resProp.getString("validPolicyMessage");
					}
				} catch (Exception e) {
					newEmail = "";
				}
				logger.info("Action Invoded:- EmailEntered :: END");
			}

			break;
			case "EmailEntered.EmailEntered-yes": {
				logger.info("Action Invoded:- EmailEntered.EmailEntered-yes :: START");
				if (responsecache_onsessionId.containsKey(sessionId)) {
					logger.info("Inside Action :- EmailEntered.EmailEntered-yes");
					String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
					String cacheEmail = mobile_email.get("newEmail") + "";
					speech = personalDetailUpdate.updateEmail(cachePolicyNo, cacheEmail);
					innerData = buttons.getButtonsYesNo(action);
				} else {
					speech = resProp.getString("validPolicyMessage");
				}
				logger.info("Action Invoded:- EmailEntered.EmailEntered-yes :: END");
			}
			break;
			case "EmailEntered.EmailEntered-no": {
				logger.info("Action Invoded:- EmailEntered.EmailEntered-no :: START");
				speech = "No problem! \n" + 
						"Is there anything else I can assist you with?";
				innerData = buttons.getButtonsYesNo(action);
				logger.info("Action Invoded:- EmailEntered.EmailEntered-no :: END");
			}
			break;
			
		/////// new CR NAV registration deregistration
			
					case "NAVRegistration":
					{
						logger.info("NAV Registration action start: ");
						if (responsecache_onsessionId.containsKey(sessionId))
						{
							String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
							speech=navRegistration.getNavRegistartion(cachePolicyNo);
							innerData = buttons.getButtonsYesNo(action);
						}
						else
						{
							speech = resProp.getString("validPolicyMessage");
						}
					}
					
					break;
					
					case "NAVDeregistration":
					{
						logger.info("NAV Deregistration action start: ");
						if (responsecache_onsessionId.containsKey(sessionId))
						{
							String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
							speech=navDeregistration.getNavDeregistartion(cachePolicyNo);
							innerData = buttons.getButtonsYesNo(action);
						}
						else
						{
							speech = resProp.getString("validPolicyMessage");
						}
					}
					break;
					
					case "NAVRegistration.NAVRegistration-yes": 
					{
						logger.info("NAVRegistration.NAVRegistration-yes action start: ");
						if (responsecache_onsessionId.containsKey(sessionId))
						{
							String transactionType="Activate";
							String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
							speech=navRegistration.navUpadte(cachePolicyNo,transactionType);
							if (speech.contains("There is some communication glitch!")) {
								speech = "There is some communication glitch! Please try again after some time. Error Code -MAX00PLD";
							} 
							else if(speech.contains("Sorry, I am unable to process your request."))
							{
							speech="Sorry, I am unable to process your request. Kindly try again after some time.</Br>" + 
							       "Alternatively, you can call our customer care on 1860-120-5577 or write to us at "                                           
							       +"service.helpdesk@maxlifeinsurance.com from your registered email id";
							}
							else {
								innerData = buttons.getButtonsYesNo(action);
							}
						}
						else
						{
							speech = resProp.getString("validPolicyMessage");
						}
					}
					break;
					
					case "NAVRegistration.NAVRegistration-no": 
					{
						logger.info("NAVRegistration.NAVRegistration-no action start: ");
						if (responsecache_onsessionId.containsKey(sessionId))
						{
							speech="No problem! </Br>" + 
									"Is there anything else I can assist you with? ";
							innerData = buttons.getButtonsYesNo(action);
						}
						else
						{
							speech = resProp.getString("validPolicyMessage");
						}
					}
					break;
					case "NAVDeregistration.NAVDeregistration-yes": 
					{
						logger.info("NAVRegistration.NAVRegistration-yes action start: ");
						if (responsecache_onsessionId.containsKey(sessionId))
						{
							String transactionType="Deactivate";
							String cachePolicyNo = responsecache_onsessionId.get(sessionId).get("PolicyNo") + "";
							speech=navRegistration.navUpadte(cachePolicyNo,transactionType);
							if (speech.contains("There is some communication glitch!")) {
								speech = "There is some communication glitch! Please try again after some time. Error Code -MAX00PLD";
							} 
							else if(speech.contains("Sorry, I am unable to process your request."))
							{
							speech="Sorry, I am unable to process your request. Kindly try again after some time.</Br>" + 
							       "Alternatively, you can call our customer care on 1860-120-5577 or write to us at "                                         
							       +"service.helpdesk@maxlifeinsurance.com from your registered email id";
							}
							else {
								innerData = buttons.getButtonsYesNo(action);
							}
						}
						else
						{
							speech = resProp.getString("validPolicyMessage");
						}
					}
					break;
					
					case "NAVDeregistration.NAVDeregistration-no": 
					{
						logger.info("NAVRegistration.NAVRegistration-no action start: ");
						if (responsecache_onsessionId.containsKey(sessionId))
						{
							speech="No problem! </Br>" + 
									"Is there anything else I can assist you with? ";
							innerData = buttons.getButtonsYesNo(action);
						}
						else
						{
							speech = resProp.getString("validPolicyMessage");
						}
					}
					break;
					
			case "PayOnline": {
				logger.info("Action Invoded:- PayOnline :: START");
				if (mapforrenewalPayment.containsKey(sessionId)) {
					logger.info("Inside Action :- PayOnline");
					speech = "We have host of online renewal payment options, you can pay renewal payment through Credit Card, EMI on Card, Net Banking, Debit Cards, Wallets /Cash card and UPI. Do you want to pay now?";
				} else {
					speech = "I haven't recognized you yet. Let me know your Policy Number so that i can verify your identity.";
				}
				logger.info("Action Invoded:- PayOnline :: END");
			}
			break;
			case "CreditCardStandingInstruction": {
				logger.info("Action Invoded:- CreditCardStandingInstruction :: START");
				if (mapforrenewalPayment.containsKey(sessionId)) {
					logger.info("Inside Action :- CreditCardStandingInstruction");
					speech = "You simply need to fill up the credit card mandate form and send it to us along with the scanned copy of the front side of your credit card at service.helpdesk@maxlifeinsurance.com. "
							+ "To download the credit card mandate form <a href='https://goo.gl/CSWygD' target='_blank'> click here. </a>  \n Is there anything else I can assist you with?";
					innerData = buttons.getButtonsYesNo(action);
				} else {
					speech = "I haven't recognized you yet. Let me know your Policy Number so that i can verify your identity.";
				}
				logger.info("Action Invoded:- CreditCardStandingInstruction :: END");
			}
			break;
			case "DirectDebit": {
				logger.info("Action Invoded:- DirectDebit :: START");
				if (mapforrenewalPayment.containsKey(sessionId)) {
					logger.info("Inside Action :- DirectDebit");
					speech = "To opt for direct debit facility, please send the duly filled ECS Mandate form along with a cancelled cheque at least 30 days before your next premium due date to us from your registered mail ID to service.helpdesk@maxlifeinsurance.com, "
							+ "alternatively you can submit the same at any of our branch offices. "
							+ "To download the direct debit mandate form "
							+ "<a href='https://goo.gl/htCdWy' target='_blank'> click here </a> \n Is there anything else I can assist you with?";
					innerData = buttons.getButtonsYesNo(action);

				} else {
					speech = "I haven't recognized you yet. Let me know your Policy Number so that i can verify your identity.";
				}
				logger.info("Action Invoded:- DirectDebit :: END");
			}
			break;
			case "OtherProcedures": {
				logger.info("Action Invoded:- OtherProcedures :: START");
				if (mapforrenewalPayment.containsKey(sessionId)) {
					logger.info("Inside Action :- OtherProcedures");
					speech = "To know more about other payment options, <a href=' https://bit.ly/2fH8ITN' target='_blank'> click here </a> \n"
							+ "\n Is there anything else I can assist you with?";
					innerData = buttons.getButtonsYesNo(action);
				} else {
					speech = "I haven't recognized you yet. Let me know your Policy Number so that i can verify your identity.";
				}
				logger.info("Action Invoded:- OtherProcedures :: END");
			}
			break;
			case "PayOnline.PayOnline-yes": {
				logger.info("Action Invoded:- PayOnline.PayOnline-yes :: START");
				if (mapforrenewalPayment.containsKey(sessionId)) {
					logger.info("Inside Action :- PayOnline.PayOnline-yes");
					speech = "<a href='https://bit.ly/1Wis1l9' target='_blank'> Click here </a> to pay your renewal payment \n Is there anything else I can assist you with?";
					innerData = buttons.getButtonsYesNo(action);
				} else {
					speech = "I haven't recognized you yet. Let me know your Policy Number so that i can verify your identity.";
				}
				logger.info("Action Invoded:- PayOnline.PayOnline-yes :: END");
			}
			break;
			case "PayOnline.PayOnline-no":
			case "credit.credit-yes":
			case "directdebit.directdebit-yes":
			case "others.others-yes":
			case "PayOnline.PayOnline-yes.PayOnline-yes-yes": {
				if (mapforrenewalPayment.containsKey(sessionId)) {
					speech = "I can help you with following options on your policy.";
					innerData = buttons.getPayOnline(action);
				} else {
					speech = "I haven't recognized you yet. Let me know your Policy Number so that i can verify your identity.";
				}
			}
			break;
			
			case "chatyes":
			case "locateus-pincode.locateus-pincode-no.locateus-pincode-no-yes": {
				speech = "Glad to serve you. Have a good Day.";
			}
			break;
			
			case "chatno":
			case "PolicyCTP.PolicyCTP-no.PolicyCTP-no-no":
			case "PolicySurrenderValue.PolicySurrenderValue-no.PolicySurrenderValue-no-no":
			case "PolicyPremiumReceipt.PolicyPremiumReceipt-no.PolicyPremiumReceipt-no-no":
			case "PolicyFundValue.PolicyFundValue-no.PolicyFundValue-no-no":
			case "InputMaturity.InputMaturity-no.InputMaturity-no-no":
			case "InputPPT.InputPPT-no.InputPPT-no-no":
			case "Policypolicydocument.Policypolicydocument-no.Policypolicydocument-no-no":
			case "locateus-pincode.locateus-pincode-no.locateus-pincode-no-no":
			case "Client-DOB.Client-DOB-no.Client-DOB-no-no":
			case "EmailEntered.EmailEntered-yes.EmailEntered-yes-no.EmailEntered-yes-no-no":
			case "MobileEntered.MobileEntered-yes.MobileEntered-yes-no.MobileEntered-yes-no-no":
			case "UpdateAadhar.UpdateAadhar-no.UpdateAadhar-no-no":
			case "BuyNow.BuyNow-no":
			case "RequestanAgent.RequestanAgent-no":
			case "KnowMoreOnline.KnowMoreOnline-no":
			case "KnowMoreOffline.KnowMoreOffline-no":
			case "AddressChange.AddressChange-no":
			case "NomineeChange.NomineeChange-no":
			case "NAVRegistration.NAVRegistration-yes.NAVRegistration-yes-no":
			case "NAVDeregistration.NAVDeregistration-yes.NAVDeregistration-yes-no":
			{
				logger.info("Inside final action :: START");
				speech = "Please let me know how helpful I was today?";
				innerData = buttons.getHelpfulSomeHelpful(action);
				logger.info("Final action :: END");
			}
			break;
			case "helpful": {
				speech = "I am glad I was of help! Thank you for reaching out to me, good day!";
			}
			break;

			case "notHelpful":
			case "SomewhatHelpful": {
				speech = "Sometimes, I may not have the information you need. I am a virtual assistant "
						+ "and I am still learning. However, thank you for your valuable feedback, "
						+ "it will help me service you better next time.\n Good day!";
			}
			break;

			case "customerexperience": {
				speech = "Thank you for your feedback, will work on improving your experience.";
			}
			break;
			case "others.others-no":
			case "credit.credit-no":
			case "directdebit.directdebit-no":
			case "PayOnline.PayOnline-yes.PayOnline-yes-no":
			case "PolicyCTP.PolicyCTP-no":
				// code added
			case "PolicySurrenderValue.PolicySurrenderValue-no":
			case "PolicyPremiumReceipt.PolicyPremiumReceipt-no":
			case "PolicyFundValue.PolicyFundValue-no":
			case "InputMaturity.InputMaturity-no":
			case "InputPPT.InputPPT-no":
			case "Policypolicydocument.Policypolicydocument-no":
			case "PolicyPolicyNumberValidation.PolicyPolicyNumberValidation-no":
			case "Client-DOB.Client-DOB-no":
			case "MobileEntered.MobileEntered-yes.MobileEntered-yes-no":
			case "EmailEntered.EmailEntered-yes.EmailEntered-yes-no":
			case "UpdateAadhar.UpdateAadhar-no": 
			case "MobileEntered.MobileEntered-no.MobileEntered-no-no":
			case "EmailEntered.EmailEntered-no.EmailEntered-no-no":
			{
				if (mapforrenewalPayment.containsKey(sessionId) || responsecache_onsessionId.containsKey(sessionId)) {
					speech = "Please let me know how helpful I was today?";
					innerData = buttons.getHelpfulSomeHelpful(action);
				} else {
					speech = "I haven't recognized you yet. Let me know your Policy Number so that i can verify your identity.";
				}
			}
			break;
			case "locateus-pincode.locateus-pincode-no": {
				action = "instafeedback-2";
			}
			break;
			case "main.options": {
				speech = resProp.getString("PolicyCTP.PolicyCTP-no");
				speech = "I can help you with following on your Policy";
				if (!"I haven't recognized you yet. Let me know your Policy Number so that i can verify your identity."
						.equalsIgnoreCase(speech)) {
					innerData = buttons.getButtonsHI(action);
				}
			}
			break;
			default:
			}
			switch (action) {
			case "instafeedback": {
				logger.info("Action Invoded:- InstaFeedBack");
				speech = "Thank you for contacting Max Life. Was the chat helpful to you ?(Yes/No)";
			}
			break;
			case "instafeedback-2": {
				logger.info("Action Invoded:- InstaFeedBack");
				speech = "Thank you for contacting Max Life. Was the chat helpful to you?(Yes/No)";
			}
			break;

			case "feedback.feedback-yes":
			case "PayOnline.PayOnline-yes.PayOnline-yes-no.PayOnline-yes-no-yes":
			case "credit.credit-no.credit-no-yes":
			case "directdebit.directdebit-no.directdebit-no-yes":
			case "others.others-no.others-no-yes":
			case "PolicyCTP.PolicyCTP-no.PolicyCTP-no-yes":
				// code added
			case "PolicySurrenderValue.PolicySurrenderValue-no.PolicySurrenderValue-no-yes":
			case "PolicyPremiumReceipt.PolicyPremiumReceipt-no.PolicyPremiumReceipt-no-yes":
			case "PolicyFundValue.PolicyFundValue-no.PolicyFundValue-no-yes":
			case "InputMaturity.InputMaturity-no.InputMaturity-no-yes":
			case "InputPPT.InputPPT-no.InputPPT-no-yes":
			case "Policypolicydocument.Policypolicydocument-no.Policypolicydocument-no-yes":
			case "Client-DOB.Client-DOB-no.Client-DOB-no-yes":
			case "EmailEntered.EmailEntered-yes.EmailEntered-yes-no.EmailEntered-yes-no-yes":
			case "MobileEntered.MobileEntered-yes.MobileEntered-yes-no.MobileEntered-yes-no-yes":
			case "UpdateAadhar.UpdateAadhar-no.UpdateAadhar-no-yes":
				//
			{
				if (mapforrenewalPayment.containsKey(sessionId) || responsecache_onsessionId.containsKey(sessionId)) {
					logger.info("Action Invoded:- feedback.feedback-yes");
					instaCallBack(sessionId);
					speech = "Glad to serve you. Have a good Day.";
				} else {
					speech = "I haven't recognized you yet. Let me know your Policy Number so that i can verify your identity.";
				}
			}
			break;
			case "chatyes":
			case "locateus-pincode.locateus-pincode-no.locateus-pincode-no-yes":
			{
				speech = "Glad to serve you. Have a good Day.";
			}
			break;

			case "chatno":
			case "PolicyCTP.PolicyCTP-no.PolicyCTP-no-no":
			case "PolicySurrenderValue.PolicySurrenderValue-no.PolicySurrenderValue-no-no":
			case "PolicyPremiumReceipt.PolicyPremiumReceipt-no.PolicyPremiumReceipt-no-no":
			case "PolicyFundValue.PolicyFundValue-no.PolicyFundValue-no-no":
			case "InputMaturity.InputMaturity-no.InputMaturity-no-no":
			case "InputPPT.InputPPT-no.InputPPT-no-no":
			case "Policypolicydocument.Policypolicydocument-no.Policypolicydocument-no-no":
			case "locateus-pincode.locateus-pincode-no.locateus-pincode-no-no":
			case "Client-DOB.Client-DOB-no.Client-DOB-no-no": {
				speech = "Please share your experience.";
			}
			break;
			case "customerexperience": {

				speech = "Thank you for your feedback, will work on improving your experience.";
			}
			break;
			case "feedback.feedback-no":
			case "PayOnline.PayOnline-yes.PayOnline-yes-no.PayOnline-yes-no-no":
			case "credit.credit-no.credit-no-no":
			case "directdebit.directdebit-no.directdebit-no-no":
			case "others.others-no.others-no-no":
			{
				logger.info("Action Invoded :- feedback.feedback-no :: START");
				if (mapforrenewalPayment.containsKey(sessionId) || responsecache_onsessionId.containsKey(sessionId)) {
					logger.info("Inside Action :- feedback.feedback-no");
					instaCallBack(sessionId);
					speech = "Thank you for your feedback, will work on improving your experience.";
				} else {
					speech = "I haven't recognized you yet. Let me know your Policy Number so that i can verify your identity.";
				}
				logger.info("Action Invoded :- feedback.feedback-no :: END");
			}
			break;
			default:
			}
		} catch (Exception e) {
			logger.error("Error in CS bot controller :: "+e);
		}
		logger.info("final Speech:-"+speech);
		WebhookResponse responseObj = new WebhookResponse(speech, speech, innerData);
		return responseObj;
	}

	public String instaCallBack(String sessionId) {
		String speech = "";
		logger.info("Inside instaCallBAck method :: START");
		if (responsecache_onsessionId.containsKey(sessionId) || mapforrenewalPayment.containsKey(sessionId)) {
			logger.info("Action Invoded:- close.conversation");
			for (Map.Entry<String, String> entry : mapforcashbeforevalidation.entrySet()) {
				String key = entry.getKey();
				if (key.equalsIgnoreCase(sessionId)) {
					mapforcashbeforevalidation.remove(sessionId);
					mapforrenewalPayment.remove(sessionId);
				}
			}
			responsecache_onsessionId.remove(sessionId);
			mapforrenewalPayment.remove(sessionId);
			logger.info("Session_Id Removed :-" + sessionId);
			speech = "Thank you for contacting Max Life. Have a great day!";
		} else {
			speech = "Thank you for contacting Max Life. Have a great day!";
		}
		return speech;
	}
}

