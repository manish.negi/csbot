package com.qc.csbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.qc" })
public class CsbotApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsbotApplication.class, args);
	}
}
